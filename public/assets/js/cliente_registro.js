$(document).ready(function(){
	
	$("form[name=registro]").on("submit", function(e){
		e.preventDefault();
		e.stopPropagation();
		var nombre = $("#nombre").val();
		var cedula = $("#cedula").val();
		var direccion = $("#direccion").val();
		var telefono = $("#telefono").val();
		var email = $("#email").val();
		var atencion = $("#asistente").val();
		cedula_aux = cedula;
		//var canal = $("#canal").val();
		//var canalOtro = $("#canal-otro").val();
		
		$.ajax({
			url: BASEURL + "/registro",
			method: $(this).attr("method"),
			type: "POST",
			dataType: 'json',
			data: {nombre: nombre, cedula: cedula, direccion : direccion, telefono: telefono, email: email, atendido: atencion},

			success:function(resp){
                
    			$("#getCode").html(resp);
    			$("#myModal").modal('show');
              }

		});
	});

});