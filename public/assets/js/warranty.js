$(document).ready(function()
{

    $("form[name=create_post]").on("submit", function(e)
    {
        e.preventDefault();
        e.stopPropagation();
        var dato = $("#cliente").val();
        var producto = $("#producto").val();
        var fecha = $("#fecha").val();
        var numero = $("#numero_contrato").val();
        $.ajax({
            url: BASEURL + "/garantias/anteriores",
            method: $(this).attr("method"),
//            data: $(this).serialize(),
            type : 'POST',
            dataType: 'json',
            data: {cliente: dato, producto: producto, fecha : fecha, numero_contrato: numero},

            success:function(){
                $("#msj-success").fadeIn();
                $('input[type="text"],textarea').val('');


            },
            error: function(XMLHttpRequest, textStatus, errorThrown) { 
                $("#msj-error").fadeIn();
            }  
            
            });
        });
    });
