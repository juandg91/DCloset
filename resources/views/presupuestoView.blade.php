<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title>D'Closet Home solutions - clientes</title>

    <!-- Bootstrap core CSS -->
    <link href="../../assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="../../assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    
    <!-- Custom styles for this template -->
    <link href="../../assets/css/style.css" rel="stylesheet">
    <link href="../../assets/css/style-responsive.css" rel="stylesheet">
    <link rel="../../stylesheet" href="assets/css/to-do.css">    



<section id="container" >
      <!-- **********************************************************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      *********************************************************************************************************************************************************** -->
      <!--header start-->
      <header class="header black-bg">
              <div class="sidebar-toggle-box">
                  <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
              </div>
            <!--logo start-->
            <a href="/" class="logo"><b>D'Closet</b></a>
            <!--logo end-->
            
            <div class="top-menu">
                <ul class="nav pull-right top-menu">
                    <li><a class="logout" href="auth/logout">Cerrar sesión</a></li>
                </ul>
            </div>

        </header>
      <!--header end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN SIDEBAR MENU
      *********************************************************************************************************************************************************** -->
      <!--sidebar start-->
     <aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu" id="nav-accordion">
              
                  <p class="centered"><a href="profile.html"><img src="../../assets/images/ui-sam.jpg" class="img-circle" width="60"></a></p>
                  <h5 class="centered">{!!$user=Auth::user()->name!!}</h5>
                    
                  <li class="mt">
                      <a class="active" href="/">
                          <i class="fa fa-dashboard"></i>
                          <span>Dashboard</span>
                      </a>
                  </li>

                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-desktop"></i>
                          <span>Clientes</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="/clientes">General</a></li>
                          <li><a  href="/clientes/trafico">Tráfico</a></li>
                          <li><a  href="/registro">Registrar nuevo</a></li> 
                      </ul>
                  </li>

                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-cogs"></i>
                          <span>Proyectos</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="/proyecto">General</a></li>
                          <li><a  href="/proyecto/nuevo">Nuevo</a></li>
                        <!--  <li><a  href="todo_list.html">Todo List</a></li> !-->
                      </ul>
                  </li>
                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-book"></i>
                          <span>Control de citas</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="/citasM">Nueva cita de medidas</a></li>
                          <li><a  href="/citasShow">Ver citas de medidas</a></li>
                          <li><a  href="blank.html">Nueva cita de presupuesto</a></li>
                          <li><a  href="/CitasPShow">Ver citas de presupuesto</a></li>
                          
                      </ul>
                  </li>
                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-tasks"></i>
                          <span>Gestión de pagos</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="/pagos/list/all">Administrar pagos</a></li>
                      </ul>
                  </li>

              </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>
      <!--sidebar end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">       
            <!-- SIMPLE TO DO LIST -->
            <div class="row mt">
              <div class="col-md-12">
                <div>

                  <center><h1>Presupuesto</h1></center>




<b>Datos del cliente:</b>
<br>
@foreach($cliente as $clienteData)

<label>Nombre: </label> {!!$clienteData->nombre!!}  
<br>
<label>Cedula: </label> {!!$clienteData->cedula!!}
<br>
<label>Direccion: </label> {!!$clienteData->direccion!!}
<br>
<label>Telefono: </label> {!!$clienteData->telefono!!}
<br>
<FORM method="POST" action="/clientes/ver/{!!$clienteData->cedula!!}"><button type="submit" class="btn btn-default"  name="ver">Ver</button></FORM>
@endforeach

@foreach($proyecto as $project)
<b><h1>Presupuesto de {!!$project->nombre!!}</h1></b>
@endforeach
<br>
<table class="table table-striped table-hover">
@foreach($presupuesto as $Presupuesto)

<tr>
<td><label>Precio del proyecto: </label></td>
  <td>{!!$project->subtotal!!} BsF</td>
  <br>
</tr>
<tr>
<td><label>Impuesto: </label></td>
  <td>{!!$project->impuesto!!} BsF</td>
</tr>
<tr>
<td><label>Total 1: </label></td>
  <td>{!!$project->total!!} BsF</td>
</tr>
<tr>
  <td><label>Descuento especial: </label></td>
  <td>{!!$Presupuesto->descuento!!} %</td>
  </tr>
  <tr>
    <tr>
    <td><label>Ahorro: </label></td>
    <td>@if($Presupuesto->ahorro == NULL)
            0 BsF
        @else
          {!!$Presupuesto->ahorro!!} BsF
        @endif
    </td>
    </tr>
  
  <tr>
  <td><label>Total a cancelar: </label></td>
  <td>{!!$Presupuesto->total!!} BsF</td>
  </tr>
  <tr>
  <td><label>Número de cuotas</label></td>
  <td>{!!$Presupuesto->cuotas!!}</td>
  </tr>
  <tr>
  <td><label>Reserva: </label></td>
  <td>{!!$Presupuesto->reserva!!} BsF</td>
  </tr>
  <tr>
  <td><label>Pago de inicial: </label></td>
  <td>{!!$Presupuesto->inicial!!} BsF</td>
  </tr>
  <tr>
  <td><label>Tiempo de ejecución: </label></td>
  <td>{!!$Presupuesto->tiempo!!} {!!$Presupuesto->UT!!}</td>
  </tr>
</table>
 
  @if($project->estado != "Planteado")
      <FORM method="GET" action="/pdf/{!!$Presupuesto->id!!}">
        <button type="submit" class="btn btn-default">Ver contrato</button>
      </FORM> 
  @endif
@endforeach  

<FORM method="GET" action="../../proyecto/update/{!!$project->id!!}">
  <button type="submit" class="btn btn-theme"><i class="fa fa-cog"></i>Editar Presupuesto</button>
  </FORM>

  <! -- MODALS -->
@if($project->estado == "Planteado")              
            <button class="btn btn-danger" data-toggle="modal" data-target="#myModal">
              Anular Proyecto
            </button>

            
            <!-- Modal -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Anular proyecto</h4>
                  </div>
                  <div class="modal-body">
                    ¿Está seguro que desea eliminar el proyecto {!!$nombre!!}? mas tarde no podrá recuperarlo
                  </div>
                  <div class="modal-footer">
                    <FORM method="POST" action="/proyecto/edit/{!!$project->id!!}/delete">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    
                    <button type="submit" class="btn btn-primary">Confirmar</button>
                    </FORM>   
                  </div>
                </div>
              </div>
            </div>              
@endif



                </div>
              </div>
            </div>
  

    </section><! --/wrapper -->
      </section><!-- /MAIN CONTENT -->

      <!--main content end-->
      <!--footer start-->
      
      </footer>
      <!--footer end-->
  </section>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="../../assets/js/jquery.js"></script>
    <script src="../../assets/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="../../assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="../../assets/js/jquery.scrollTo.min.js"></script>
    <script src="../../assets/js/jquery.nicescroll.js" type="text/javascript"></script>


    <!--common script for all pages-->
    <script src="../../assets/js/common-scripts.js"></script>

    <!--script for this page-->
  <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>    
    <script src="../../assets/js/tasks.js" type="text/javascript"></script>

    <script>
      jQuery(document).ready(function() {
          TaskList.initTaskWidget();
      });

      $(function() {
          $( "#sortable" ).sortable();
          $( "#sortable" ).disableSelection();
      });

    </script>
    
    
  <script>
      //custom select box

      $(function(){
          $('select.styled').customSelect();
      });

  </script>