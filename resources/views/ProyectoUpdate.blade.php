<?php

  foreach ($project as $projects) {
    $name = $projects->nombre;
    $producto = $projects->producto;
    $subtotal = $projects->subtotal;
    $impuesto = $projects->impuesto;
    $total = $projects->total;
    $obs = $projects->obs;
  }

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title>D'Closet Home solutions - clientes</title>

    <!-- Bootstrap core CSS -->
    <link href="../../assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="../../assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    
    <!-- Custom styles for this template -->
    <link href="../../assets/css/style.css" rel="stylesheet">
    <link href="../../assets/css/style-responsive.css" rel="stylesheet">
    <link rel="../../stylesheet" href="assets/css/to-do.css">
  </head>

  <body>

  <section id="container" >
      <!-- **********************************************************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      *********************************************************************************************************************************************************** -->
      <!--header start-->
     <header class="header black-bg">
              <div class="sidebar-toggle-box">
                  <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
              </div>
            <!--logo start-->
            <a href="/" class="logo"><b>D'Closet</b></a>
            <!--logo end-->
            
            <div class="top-menu">
                <ul class="nav pull-right top-menu">
                    <li><a class="logout" href="auth/logout">Cerrar sesión</a></li>
                </ul>
            </div>

        </header>
      <!--header end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN SIDEBAR MENU
      *********************************************************************************************************************************************************** -->
      <!--sidebar start-->
      <aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu" id="nav-accordion">
              
                  <p class="centered"><a href="profile.html"><img src="../../assets/images/ui-sam.jpg" class="img-circle" width="60"></a></p>
                  <h5 class="centered">{!!$user=Auth::user()->name!!}</h5>
                    
                  <li class="mt">
                      <a class="active" href="index.html">
                          <i class="fa fa-dashboard"></i>
                          <span>Dashboard</span>
                      </a>
                  </li>

                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-desktop"></i>
                          <span>Clientes</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="../../clientes">General</a></li>
                          <li><a  href="../../clientes/trafico">Tráfico</a></li>
                          <li><a  href="../../registro">Registrar nuevo</a></li> 
                      </ul>
                  </li>

                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-cogs"></i>
                          <span>Proyectos</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="../../proyecto">General</a></li>
                          <li><a  href="../../proyecto/nuevo">Nuevo</a></li>
                        <!--  <li><a  href="todo_list.html">Todo List</a></li> !-->
                      </ul>
                  </li>
                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-book"></i>
                          <span>Control de citas</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="../../citasM">Nueva cita de medidas</a></li>
                          <li><a  href="../../citasShow">Ver citas de medidas</a></li>
                          <li><a  href="blank.html">Nueva cita de presupuesto</a></li>
                          <li><a  href="../../CitasPShow">Ver citas de presupuesto</a></li>
                          
                      </ul>
                  </li>
                  

              </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>
      <!--sidebar end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
            <h3><i class="fa fa-angle-right"></i> {!!$name!!}</h3>
            
            <!-- BASIC FORM ELELEMNTS -->
            <div class="row mt">
              <div class="col-lg-12">
                  <div class="form-panel">
                      <h4 class="mb"><i class="fa fa-angle-right"></i> Datos del proyecto</h4>
                      <form class="form-horizontal style-form" method="POST">
                          
                          
                          <div class="form-group">
                            <label class="col-sm-2 col-sm-2 control-label">Nombre del proyecto</label> 
                              <div class="col-sm-10">
                                <input type="text" name="nombre" class="form-control round-form" placeholder="Nombre del nuevo proyectos" value="{!!$name!!}" required>
                              </div>
                          </div>
                          <div class="form-group">
                          <label class="col-sm-2 col-sm-2 control-label">Producto</label>
                          <div class="col-sm-10"> 
                            <select class="form-control" name="product" id="product" onchange="prod(this)" placeholder="Tipo de producto" required>    
                            <option value="{!!$producto!!}">{!!$producto!!}</option>        
                          </select>
                          </div>
                          </div>

                          <div class="form-group">
                          <label class="col-sm-2 col-sm-2 control-label">Linea</label>
                          <div class="col-sm-10"> 
                            <select class="form-control" name="linea" id="line" onchange="return linea(this)" required>
                            @foreach($linea as $value) 
                            <option value="{!!$value->nombre!!}">{!!$value->nombre!!}</option>
                            @endforeach
                            </select>
                          </div>
                          </div>

                          <div class="form-group">
                          <label class="col-sm-2 col-sm-2 control-label">Modelo</label>
                          <div class="col-sm-10"> 
                            <select class="form-control" name="modelo">
                            <option value="platinum">Platinum</option>
                            <option value="gold">Gold</option>
                            </select>
                          </div>
                          </div>
                          @if($producto != "vestier")
                          <div class="form-group">
                          <label class="col-sm-2 col-sm-2 control-label">Puertas</label>
                          <div class="col-sm-10">
                          <select class="form-control" name="puertas">
                          <option value="Batiente">Batiente</option>
                          <option value="Libro">Libro</option>
                          <option value="Basculante">Basculante</option>
                          </select>
                          </div>
                          </div>
                          @endif
                          <div class="form-group">
                          <label class="col-sm-2 col-sm-2 control-label">Manillas</label>
                          <div class="col-sm-10">
                          <select class="form-control" name="manillas" id="manillas" required>
                          <option value="Acero inoxidable">Acero inoxidable</option>
                          <option value="Metalica">Metalicas</option>
                          <option value="Plastico">Plastico</option>
                          </select>
                        </div>
                        </div>
                        @if($producto == "cocina")
                        <div class="form-group" id="Struct">
                        <label class="col-sm-2 col-sm-2 control-label">Estructura interna</label>
                        <div class="col-sm-10">
                        <select class="form-control" name="struct" id="manillas">
                        <option value="Aglomerado">Aglomerado</option>
                        </select>
                        </div>
                        </div>
                        @endif
                        <div class="form-group" id="color">
                        <label class="col-sm-2 col-sm-2 control-label">Color</label>
                        <div class="col-sm-10">
                        <select class="form-control" name="color" id="color" required>
                        <option value="Blanco">Blanco</option>
                        <option value="Negro">Negro</option>
                        <option value="Gris">Gris</option>
                        <option value="Amarillo">Amarillo</option>
                        <option value="Rojo">Rojo</option>
                        <option value="Verde">Verde</option>
                        <option value="Marron">Caoba</option>
                      </select>
                    </div>
                    </div>

                    <div id="tope-div">
                    <div class="form-group" style.display:"hidden">
                    <label class="col-sm-2 col-sm-2 control-label">Tope</label>
                    <div class="col-sm-10">
                    <select class="form-control" name="tope">
                      <option value="wall">Wall</option>
                      <option value="Midwall">Mid Wall</option>
                    </select>
                  </div>
                </div>
                 <div class="form-group" style.display:"hidden">
                <label class="col-sm-2 col-sm-2 control-label">Material: </label>
                <div class="col-sm-10">
                <select class="form-control round-form" name="tope_mat" id="tope" required>
                <option value="granito">Granito</option>
                <option value="stone">Stone</option>
                </select>
                </div>
                </div>
                </div>

                <div class="form-group">
                <label class="col-sm-2 col-sm-2 control-label">Subtotal</label> 
                <div class="col-sm-10">
                <input type="text" id="subt" name="subtotal" class="form-control round-form" placeholder="Subtotal" onchange="taxes(this.value)" value="{!!$subtotal!!}">
                </div>
                </div>

                <div class="form-group">
                <label class="col-sm-2 col-sm-2 control-label">Impuesto</label>
                <div class="col-sm-10"> 
                <input type="text" id="imp" name="impuesto" class="form-control round-form" value="{!!$impuesto!!}">
                </div>
                </div>

                <div class = "form-group">
                <label class="col-sm-2 col-sm-2 control-label">Total</label>
                <div class="col-sm-10">  
                <input type="text" id="total" name="total" class="form-control round-form" value="{!!$total!!}">
                </div>
                </div > 
                    
                <div class="form-group">
                <label class="col-sm-2 col-sm-2 control-label">Observaciones</label>
                <div class="col-sm-10"> 
                <input type="text" name="obs" class="form-control round-form" placeholder="Observaciones" value="{!!$obs!!}" required>
                </div>
                </div>

                <button type="submit" class="btn btn-primary active">Guardar cambios</button> 
                      </form>
                  </div>
              </div><!-- col-lg-12-->       
            </div><!-- /row -->
         
            
    </section><! --/wrapper -->
      </section><!-- /MAIN CONTENT -->

      <!--main content end-->
      <!--footer start-->
      <footer class="site-footer">
          <div class="text-center">
              2016 - Cumaná
              <a href="form_component.html#" class="go-top">
                  <i class="fa fa-angle-up"></i>
              </a>
          </div>
      </footer>
      <!--footer end-->
  </section>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>
    <script src="assets/js/jquery.nicescroll.js" type="text/javascript"></script>


    <!--common script for all pages-->
    <script src="assets/js/common-scripts.js"></script>

    <!--script for this page-->
    <script src="assets/js/jquery-ui-1.9.2.custom.min.js"></script>

  <!--custom switch-->
  <script src="assets/js/bootstrap-switch.js"></script>
  
  <!--custom tagsinput-->
  <script src="assets/js/jquery.tagsinput.js"></script>
  
  <!--custom checkbox & radio-->
  
  <script type="text/javascript" src="assets/js/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
  <script type="text/javascript" src="assets/js/bootstrap-daterangepicker/date.js"></script>
  <script type="text/javascript" src="assets/js/bootstrap-daterangepicker/daterangepicker.js"></script>
  
  <script type="text/javascript" src="assets/js/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
  
  
  <script src="assets/js/form-component.js"></script>    
    
    
  <script>
      //custom select box

      $(function(){
          $('select.styled').customSelect();
      });

  </script>

  </body>
</html>




<script type="text/javascript">
  
  function prod(tipo)
  {

      if(tipo.value == 'closet')
      {
        
          document.getElementById('tope-div').style.visibility = 'hidden';
          document.getElementById('Struct').style.visibility = 'hidden'; 
          document.getElementById('puertas').style.visibility = 'visible';
      }
      if(tipo.value == 'vestier')
      {
        document.getElementById('puertas').style.visibility = 'hidden';
        document.getElementById('tope-div').style.visibility = 'hidden';
        document.getElementById('Struct').style.visibility = 'hidden'; 
      }
      if(tipo.value == 'cocina')
      {
        document.getElementById('puertas').style.visibility = 'visible';
        document.getElementById('tope-div').style.visibility = 'visible';
        document.getElementById('Struct').style.visibility = 'visible'; 
      }
  }

  function taxes(mount)
  {
      document.getElementById("imp").value = mount*0.12;
      document.getElementById("total").value =  parseFloat(document.getElementById("imp").value) + parseFloat(mount);
  }
  
</script>
<script type="text/javascript" src="assets/js/dropdown.js"></script>
