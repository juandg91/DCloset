<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title>DCloset Home solutions</title>

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
        
    <!-- Custom styles for this template -->
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/style-responsive.css" rel="stylesheet">

    <link href="assets/css/table-responsive.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

  <section id="container" >
      <!-- **********************************************************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      *********************************************************************************************************************************************************** -->
      <!--header start-->
      <header class="header black-bg">
              <div class="sidebar-toggle-box">
                  <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
              </div>
            <!--logo start-->
            <a href="/" class="logo"><b>D'Closet</b></a>
            <!--logo end-->
            <div class="nav notify-row" id="top_menu">
                <!--  notification start -->
                <ul class="nav top-menu">
                    <!-- settings start -->
                    
                    <!-- settings end -->
                    <!-- inbox dropdown start-->
                   
                    <!-- inbox dropdown end -->
                </ul>
                <!--  notification end -->
            </div>
            <div class="top-menu">
                <ul class="nav pull-right top-menu">
                    <li><a class="logout" href="auth/logout">Cerrar sesión</a></li>
                </ul>
            </div>
        </header>
              <!--header end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN SIDEBAR MENU
      *********************************************************************************************************************************************************** -->
      <!--sidebar start-->
      <aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu" id="nav-accordion">
              
                  <p class="centered"><a href="profile.html"><img src="assets/images/ui-sam.jpg" class="img-circle" width="60"></a></p>
                  <h5 class="centered">{!!$user=Auth::user()->name!!}</h5>
                    
                  <li class="mt">
                      <a class="active" href="index.html">
                          <i class="fa fa-dashboard"></i>
                          <span>Dashboard</span>
                      </a>
                  </li>

                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-desktop"></i>
                          <span>Clientes</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="clientes">General</a></li>
                          <li><a  href="clientes/trafico">Tráfico</a></li>
                          <li><a  href="registro">Registrar nuevo</a></li> 
                      </ul>
                  </li>

                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-cogs"></i>
                          <span>Proyectos</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="proyecto">General</a></li>
                          <li><a  href="proyecto/nuevo">Nuevo</a></li>
                        <!--  <li><a  href="todo_list.html">Todo List</a></li> !-->
                      </ul>
                  </li>
                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-book"></i>
                          <span>Control de citas</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="citasM">Nueva cita de medidas</a></li>
                          <li><a  href="citasShow">Ver citas de medidas</a></li>
                          <li><a  href="blank.html">Nueva cita de presupuesto</a></li>
                          <li><a  href="CitasPShow">Ver citas de presupuesto</a></li>
                          
                      </ul>
                  </li>
                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-tasks"></i>
                          <span>Gestión de pagos</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="/pagos/list/all">Administrar pagos</a></li>
                      </ul>
                  </li>

              </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>
      <!--sidebar end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
          	<h3><i class="fa fa-angle-right"></i>Control de citas de {!!$tipo!!}</h3>

          	<br><br>
			@if($tipo == "presupuesto")
			<form role="form" class="form-inline form-actions" method="POST" action="\CitasSearchP">
		
			<input type="search" placeholder="buscar" class="form-search" name="search" required>
		
			<button type="submit" class="btn">Buscar</button>
		
			</form>
      <br>
      <div class="showback">
                <h4><i class="fa fa-angle-right"></i> Ordenar</h4>
            <div class="btn-group btn-group-justified">
              <div class="btn-group">
                <form method="GET" action="/citasPShowPend">
                <button type="submit" class="btn btn-theme">Pendientes</button>
                </form>
              </div>
              <div class="btn-group">
                <form method="GET" action="/citasPShowDone">
                <button type="submit" class="btn btn-theme">Realizadas</button>
                </form>
              </div>
              <div class="btn-group">
                <form method="GET" action="/citasPShowOrdered">
                <button type="submit" class="btn btn-theme">Por fecha</button>
                </form>
              </div>
              <div class="btn-group">
                <form method="GET" action="/CitasPShow">
                <button type="submit" class="btn btn-theme">Todas</button>
                </form>
              </div>
            </div>              
              </div><!--/showback -->
			@endif
			@if($tipo == "medidas")
			<form role="form" class="form-inline form-actions" method="POST" action="\CitasSearch">
		
			<input type="search" placeholder="buscar" class="form-search" name="search" required>
		
			<button type="submit" class="btn">Buscar</button>
		
			</form>
      <br>
      <div class="showback">
                <h4><i class="fa fa-angle-right"></i> Ordenar</h4>
            <div class="btn-group btn-group-justified">
              <div class="btn-group">
                <form method="GET" action="/citasShowPend">
                <button type="submit" class="btn btn-theme">Pendientes</button>
                </form>
              </div>
              <div class="btn-group">
                <form method="GET" action="/citasShowDone">
                <button type="submit" class="btn btn-theme">Realizadas</button>
                </form>
              </div>
              <div class="btn-group">
                <form method="GET" action="/citasShowOrdered">
                <button type="submit" class="btn btn-theme">Por fecha</button>
                </form>
              </div>
              <div class="btn-group">
                <form method="GET" action="/citasShow">
                <button type="submit" class="btn btn-theme">Todas</button>
                </form>
              </div>
            </div>              
              </div><!--/showback -->
			@endif
			
		  		<div class="row mt">
			  		<div class="col-lg-12">
                      <div class="content-panel">
                      <h4><i class="fa fa-angle-right"></i> Citas</h4>
                          <section id="unseen">
                            <table class="table table-bordered table-striped table-condensed">
                              <thead>
                              <tr>
                              	  <th>Nombre</th>
                                  <th>Cédula</th>
                                  <th>Dirección</th>
                                  <th class="numeric">Telefono</th>
                                  <th>Estado</th>
                                  <th>Fecha</th>
                                  <th>Hora</th>
                                  <th>Observaciones</th>
                                  <th>Fecha de registro</th>
                                  <th>Acción</th>
                              </tr>
                              </thead>
                              <tbody>
                              @foreach  ($cita as $citas)
                              <tr>
                              	@foreach ($cliente_cita as $cliente_citas)

								                @foreach ($cliente as $clientes)
                                  @if ($cliente_citas->cedula_cliente == $clientes->cedula)
              									@if ($cliente_citas->id_cita == $citas->id)
              									<td>{!! $clientes->nombre !!}</td>
              									<td>{!! $clientes->cedula !!}</td>
              									<td>{!! $clientes->direccion !!}</td>
              									<td class="numeric">{!! $clientes->telefono !!}</td>
              									<td>{!! $citas->estado !!}</td>
              									<td>{!!$citas->fecha!!}</td>
              									<td>{!!$citas->hora!!}</td>
              									<td>{!!$citas->obs!!}</td>
              									<td>{!!$citas->created_at!!}</td>
              									@if($tipo == "medidas")

              									<td><FORM method="POST" action="/citaEdit/{!! $clientes->cedula !!}/{!! $cliente_citas->id_cita !!}"><button type="submit" class="btn btn-default" value="{!! $clientes->cedula !!}"  name="editar">Editar</button></FORM></td></tr>
              									@endif

              									@if($tipo == "presupuesto")
              									<td><FORM method="GET" action="/citapEdit/{!! $clientes->cedula !!}/{!! $cliente_citas->id_cita !!}"><button type="submit" class="btn btn-default" value="{!! $clientes->cedula !!}"  name="editar">Editar</button></FORM></td></tr>
              									@endif

              									@endif
              									@endif

                                @endforeach
                                @endforeach  
                              </tr>
                              @endforeach
                              
                              </tbody>
                          </table>
                          </section>
                  </div><!-- /content-panel -->
               </div><!-- /col-lg-4 -->			
		  	</div><!-- /row -->

		</section><! --/wrapper -->
      </section><!-- /MAIN CONTENT -->

      <!--main content end-->
      <!--footer start-->
      <footer class="site-footer">
          <div class="text-center">
              2016 - Cumaná
              <a href="responsive_table.html#" class="go-top">
                  <i class="fa fa-angle-up"></i>
              </a>
          </div>
      </footer>
      <!--footer end-->
  </section>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>
    <script src="assets/js/jquery.nicescroll.js" type="text/javascript"></script>


    <!--common script for all pages-->
    <script src="assets/js/common-scripts.js"></script>

    <!--script for this page-->
    

  </body>
</html>