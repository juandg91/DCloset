<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title>DCloset Home solutions</title>

    <!-- Bootstrap core CSS -->
    <link href="../../assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="../../assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
        
    <!-- Custom styles for this template -->
    <link href="../../assets/css/style.css" rel="stylesheet">
    <link href="../../assets/css/style-responsive.css" rel="stylesheet">

    <link href="../../assets/css/table-responsive.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>

  <section id="container" >
      <!-- **********************************************************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      *********************************************************************************************************************************************************** -->
      <!--header start-->
      <header class="header black-bg">
              <div class="sidebar-toggle-box">
                  <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
              </div>
            <!--logo start-->
            <a href="/" class="logo"><b>D'Closet</b></a>
            <!--logo end-->
            <div class="nav notify-row" id="top_menu">
                <!--  notification start -->
                <ul class="nav top-menu">
                    <!-- settings start -->
                    
                    <!-- settings end -->
                    <!-- inbox dropdown start-->
                   
                    <!-- inbox dropdown end -->
                </ul>
                <!--  notification end -->
            </div>
            <div class="top-menu">
                <ul class="nav pull-right top-menu">
                    <li><a class="logout" href="auth/logout">Cerrar sesión</a></li>
                </ul>
            </div>
        </header>
      <!--header end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN SIDEBAR MENU
      *********************************************************************************************************************************************************** -->
      <!--sidebar start-->
      <aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu" id="nav-accordion">
              
                  <p class="centered"><a href="profile.html"><img src="../../assets/images/ui-sam.jpg" class="img-circle" width="60"></a></p>
                  <h5 class="centered">{!!$user=Auth::user()->name!!}</h5>
                    
                  <li class="mt">
                      <a class="sub" href="/">
                          <i class="fa fa-dashboard"></i>
                        <span>Dashboard</span></a>   
                      
                  </li>
                  
                  <li class="sub-menu">
                      <a class="sub" href="/clientes">
                          <i class="fa fa-desktop"></i>
                        <span>Clientes</span></a>   
                      
                  </li>
                  
                  <li class="sub-menu">
                      <a class="sub" href="/proyecto">
                          <i class="fa fa-cogs"></i>
                        <span>Proyectos</span></a>   
                      
                  </li>
                  
                  <li class="sub-menu">
                      <a class="active" href="/citas">
                          <i class="fa fa-book"></i>
                        <span>Citas</span></a>   
                  </li>

                  <li class="sub-menu">
                      <a class="sub" href="/pagos/list/all">
                          <i class="fa fa-book"></i>
                        <span>Gestión de pagos</span></a>   
                  </li>

              </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>
      <!--sidebar end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
          	<h3><i class="fa fa-angle-right"></i> Control de citas de {!!$type!!}</h3>
          	
          	<!-- BASIC FORM ELELEMNTS -->
          	<div class="row mt">
          		<div class="col-lg-12">
                  <div class="form-panel">
                  	  <h4 class="mb"><i class="fa fa-angle-right"></i> Editar cita</h4>
                  	  @if($type == "medidas")
						<form name="statusCita" class="form-horizontal style-form" method="POST" action ="/citaEdit/{!!$cedula!!}/{!!$id!!}/confirm">
					  @endif
					  @if($type == "presupuesto")
						<form name="statusCita" class="form-horizontal style-form" method="POST" action ="/citapEdit/{!!$cedula!!}/{!!$id!!}/confirm">
					  @endif
                      		<hr>
                          <select name="status" class="form-control" onchange="return mostrar(this)" required>
							<option selected disabled>Selecciona el nuevo estado</option>
							<option>Pendiente</option>
							<option>Pospuesta</option>
							<option>Realizada</option>
							<option>Cancelada</option>
							</select>

							<br>

							
							<div class="form-group" id="obs-div">
							<label name="obsLabel" class="col-sm-2 col-sm-2 control-label">Observaciones</label>
							<div class="col-sm-10">
							<input type="text" name="obs" id="observaciones" class="form-control" required>
							</div>
							</div>
	
							<div class="form-group" id="date-div">
							<label name="fechaLabel" class="col-sm-2 col-sm-2 control-label">Fecha</label> 
							<input type="date" name="fecha" id="fecha2">
							</div>
							<div class= "form-group" id="hour-div">
							<label name="horaLabel" class="col-sm-2 col-sm-2 control-label">Hora</label> 
							<input type="time" name="hora" id="time2">
							</div>

							<button class="btn" type="submit">Aceptar</button>
							
                        
                      </form>
                  </div>
          		</div><!-- col-lg-12-->      	
          	</div><!-- /row -->
          	
          	
		</section><! --/wrapper -->
      </section><!-- /MAIN CONTENT -->

      <!--main content end-->
      <!--footer start-->
      <footer class="site-footer">
          <div class="text-center">
              2016 - Cumaná
              <a href="form_component.html#" class="go-top">
                  <i class="fa fa-angle-up"></i>
              </a>
          </div>
      </footer>
      <!--footer end-->
  </section>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>
    <script src="assets/js/jquery.nicescroll.js" type="text/javascript"></script>


    <!--common script for all pages-->
    <script src="assets/js/common-scripts.js"></script>

    <!--script for this page-->
    <script src="assets/js/jquery-ui-1.9.2.custom.min.js"></script>

	<!--custom switch-->
	<script src="assets/js/bootstrap-switch.js"></script>
	
	<!--custom tagsinput-->
	<script src="assets/js/jquery.tagsinput.js"></script>
	
	<!--custom checkbox & radio-->
	
	<script type="text/javascript" src="assets/js/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
	<script type="text/javascript" src="assets/js/bootstrap-daterangepicker/date.js"></script>
	<script type="text/javascript" src="assets/js/bootstrap-daterangepicker/daterangepicker.js"></script>
	
	<script type="text/javascript" src="assets/js/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
	
	
	<script src="assets/js/form-component.js"></script>    
    
    
  <script>
      //custom select box

      $(function(){
          $('select.styled').customSelect();
      });

  </script>

  </body>
</html>


<script type="text/javascript">

	function initialize()
	{
		Obsdiv.style.visibility = 'hidden';
		Datediv.style.visibility = 'hidden';
	}	

	function mostrar(obj)
	{

		Obsdiv = document.getElementById("obs-div");
		Datediv = document.getElementById("date-div");
		Hourdiv = document.getElementById("hour-div");

		if(obj.value=="Pospuesta")
		{
			Obsdiv.style.visibility = 'visible';
			Datediv.style.visibility = 'visible';
			Hourdiv.style.visibility = 'visible';
			
		}
		if(obj.value=="Realizada")
		{
			Obsdiv.style.visibility = 'visible';
			Datediv.style.visibility = 'hidden';
			Hourdiv.style.visibility = 'hidden';
		}
		if(obj.value=="Pendiente")
		{
			Obsdiv.style.visibility = 'hidden';
			Datediv.style.visibility = 'hidden';
			Hourdiv.style.visibility = 'hidden';
		}
		if(obj.value=="Cancelada")
		{
			Obsdiv.style.visibility = 'visible';
			Datediv.style.visibility = 'hidden';
			Hourdiv.style.visibility = 'hidden';
		}
	}

</script>
