<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title>D'Closet Home solutions - Gestión de pagos</title>

    <!-- Bootstrap core CSS -->
    <link href="/assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    
    <!-- Custom styles for this template -->
    <link href="/assets/css/style.css" rel="stylesheet">
    <link href="/assets/css/style-responsive.css" rel="stylesheet">
    <link rel="/stylesheet" href="assets/css/to-do.css">    



<section id="container" >
      <!-- **********************************************************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      *********************************************************************************************************************************************************** -->
      <!--header start-->
      <header class="header black-bg">
              <div class="sidebar-toggle-box">
                  <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
              </div>
            <!--logo start-->
            <a href="/" class="logo"><b>D'Closet</b></a>
            <!--logo end-->
            
            <div class="top-menu">
                <ul class="nav pull-right top-menu">
                    <li><a class="logout" href="auth/logout">Cerrar sesión</a></li>
                </ul>
            </div>

        </header>
      <!--header end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN SIDEBAR MENU
      *********************************************************************************************************************************************************** -->
      <!--sidebar start-->
     <aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu" id="nav-accordion">
              
                  <p class="centered"><a href="profile.html"><img src="/assets/images/ui-sam.jpg" class="img-circle" width="60"></a></p>
                  <h5 class="centered">{!!$user=Auth::user()->name!!}</h5>
                    
                  <li class="mt">
                      <a class="sub" href="/">
                          <i class="fa fa-dashboard"></i>
                        <span>Dashboard</span></a>   
                      
                  </li>
                  
                  <li class="sub-menu">
                      <a class="sub" href="/clientes">
                          <i class="fa fa-desktop"></i>
                        <span>Clientes</span></a>   
                      
                  </li>
                  
                  <li class="sub-menu">
                      <a class="sub" href="/proyecto">
                          <i class="fa fa-cogs"></i>
                        <span>Proyectos</span></a>   
                      
                  </li>
                  
                  <li class="sub-menu">
                      <a class="sub" href="/citas">
                          <i class="fa fa-book"></i>
                        <span>Citas</span></a>   
                  </li>

                  <li class="sub-menu">
                      <a class="active" href="/pagos/list/all">
                          <i class="fa fa-book"></i>
                        <span>Gestión de pagos</span></a>   
                  </li>
              </ul>

                  
              <!-- sidebar menu end-->
          </div>
      </aside>
      <!--sidebar end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">       
            <!-- SIMPLE TO DO LIST -->
            <div class="row mt">
              <div class="col-md-12">
                <div>

              <center><h1>Control de pagos</h1></center>

              <b><h1>Cancelados</h1></b>
              
              <div class="row mt">
              <div class="col-lg-12">
                <div class="form-panel">
                      <form class="form-inline" role="form" method="POST" action="/pagos/list/all/canceled/datefilter">
                          <div class="form-group">
                              <label class="sr-only">Mes</label>
                              <select class="form-control" name="month">
                              <option value="01">Enero</option>
                              <option value="02">Febrero</option>
                              <option value="03">Marzo</option>
                              <option value="04">Abril</option>
                              <option value="05">Mayo</option>
                              <option value="06">Junio</option>
                              <option value="07">Julio</option>
                              <option value="08">Agosto</option>
                              <option value="09">Septiembre</option>
                              <option value="10">Octubre</option>
                              <option value="11">Noviembre</option>
                              <option value="12">Diciembre</option>
                            </select>
                          </div>
                          <div class="form-group">
                              <label class="sr-only" for="exampleInputPassword2">Año</label>
                              <select class="form-control">
                              <option value="2016">2016</option>
                            </select>
                          </div>
                          <button type="submit" class="btn">Filtrar</button>
                      </form>
                      <form class="form-inline" method="POST" action="/pagos/list/all/canceled/clientfilter">
                        <div class="form-group">
                              <label class="col-sm-3 col-sm-3 control-label">Buscar cliente</label>
                              
                                  <input type="text" name="cliente" class="form-control round-form" placeholder="cédula de identidad o nombre del cliente">
                                 
                              
                        </div>
                        <button type="submit" class="btn">Buscar</button>
                      </form>
                </div><!-- /form-panel -->
              </div><!-- /col-lg-12 -->
            </div><!-- /row -->
            <br>

              <table class="table table-striped table-hover">

              <tr>
              <td><b>Fecha</b></td><td><b>Monto</b></td><td><b>Forma de pago</b></td><td><b>Cliente</b></td><td><b>Concepto</b></td><td>Banco</td><td>ref</td></tr>
              @foreach($pagos as $pago)
              @if($pago->estado == "cancelado") 
              <tr>
              <td>{!!$pago->fecha_abono!!}</td>
              <td>{!!$pago->abono!!} BsF</td>
              <td>{!!$pago->forma!!}</td>

              @foreach($cliente as $clientes)
                @foreach($cPago as $cPagos)
                  @if($pago->id == $cPagos->id_pago)
                    @if($cPagos->cedula_cliente == $clientes->cedula)
                      <td>{!!$clientes->nombre!!}</td>
                    @endif
                  @endif
                  
                @endforeach
              @endforeach
              <td>{!!$pago->concepto!!}</td>
              <td>
                @if($pago->forma == "Efectivo")
                  N/A
                @else  
                  @if($pago->banco != NULL)
                  {!!$pago->banco!!}
                  @else 
                  No registrado
                  @endif
                @endif  
              </td>
              <td>
                @if($pago->forma == "Efectivo")
                  N/A
                @else
                  @if($pago->ref != NULL)
                  {!!$pago->ref!!}
                  @else 
                  No registrado
                  @endif
                @endif  
              </td> 
              <td>
                <form method="GET">
                <button type="submit" class="btn btn-theme"><i class="fa fa-cog"></i>Imprimir recibo</button>
                </form>
                </td>
              
              </tr>
              @endif
              @endforeach
              </table>

                </div>
              </div>
            </div>
  

    </section><! --/wrapper -->
      </section><!-- /MAIN CONTENT -->

      <!--main content end-->
      <!--footer start-->
      
      </footer>
      <!--footer end-->
  </section>

    <!-- js placed at the end of the document so the pages load faster -->
    
    <script src="assets/js/jjquery-1.8.3.min.js"></script>
    
    <script src="../../assets/js/jquery.js"></script>
    <script src="../../assets/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="../../assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="../../assets/js/jquery.scrollTo.min.js"></script>
    <script src="../../assets/js/jquery.nicescroll.js" type="text/javascript"></script>


    <!--common script for all pages-->
    <script src="../../assets/js/common-scripts.js"></script>

    <!--script for this page-->
  <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>    
    <script src="../../assets/js/tasks.js" type="text/javascript"></script>

    <script>
      jQuery(document).ready(function() {
          TaskList.initTaskWidget();
      });

      $(function() {
          $( "#sortable" ).sortable();
          $( "#sortable" ).disableSelection();
      });

    </script>
    
    
  <script>
      //custom select box

      $(function(){
          $('select.styled').customSelect();
      });

  </script>