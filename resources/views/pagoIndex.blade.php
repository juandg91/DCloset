<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title>D'Closet Home solutions - Gestión de pagos</title>

    <!-- Bootstrap core CSS -->
    <link href="../../assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="../../assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    
    <!-- Custom styles for this template -->
    <link href="../../assets/css/style.css" rel="stylesheet">
    <link href="../../assets/css/style-responsive.css" rel="stylesheet">
    <link rel="../../stylesheet" href="assets/css/to-do.css">    



<section id="container" >
      <!-- **********************************************************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      *********************************************************************************************************************************************************** -->
      <!--header start-->
      <header class="header black-bg">
              <div class="sidebar-toggle-box">
                  <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
              </div>
            <!--logo start-->
            <a href="/" class="logo"><b>D'Closet</b></a>
            <!--logo end-->
            
            <div class="top-menu">
                <ul class="nav pull-right top-menu">
                    <li><a class="logout" href="auth/logout">Cerrar sesión</a></li>
                </ul>
            </div>

        </header>
      <!--header end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN SIDEBAR MENU
      *********************************************************************************************************************************************************** -->
      <!--sidebar start-->
     <aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu" id="nav-accordion">
              
                  <p class="centered"><a href="profile.html"><img src="../../assets/images/ui-sam.jpg" class="img-circle" width="60"></a></p>
                  <h5 class="centered">{!!$user=Auth::user()->name!!}</h5>
                    
                  <li class="mt">
                      <a class="active" href="index.html">
                          <i class="fa fa-dashboard"></i>
                          <span>Dashboard</span>
                      </a>
                  </li>

                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-desktop"></i>
                          <span>Clientes</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="../../clientes">General</a></li>
                          <li><a  href="../../clientes/trafico">Tráfico</a></li>
                          <li><a  href="../../registro">Registrar nuevo</a></li> 
                      </ul>
                  </li>

                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-cogs"></i>
                          <span>Proyectos</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="../../proyecto">General</a></li>
                          <li><a  href="../../proyecto/nuevo">Nuevo</a></li>
                        <!--  <li><a  href="todo_list.html">Todo List</a></li> !-->
                      </ul>
                  </li>
                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-book"></i>
                          <span>Control de citas</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="../../citasM">Nueva cita de medidas</a></li>
                          <li><a  href="../../citasShow">Ver citas de medidas</a></li>
                          <li><a  href="blank.html">Nueva cita de presupuesto</a></li>
                          <li><a  href="../../CitasPShow">Ver citas de presupuesto</a></li>
                          
                      </ul>
                  </li>
                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-tasks"></i>
                          <span>Gestión de pagos</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="/pagos/list/all">Administrar pagos</a></li>
                      </ul>
                  </li>

              </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>
      <!--sidebar end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">       
            <!-- SIMPLE TO DO LIST -->
            <div class="row mt">
              <div class="col-md-12">
                <div>

                  <center><h1>Control de pagos</h1></center>


              <b><h1>Cancelados</h1></b>
              <br>
              <table class="table table-striped table-hover">

              <tr>
              <td><b>Fecha</b></td><td><b>Monto</b></td><td><b>Forma de pago</b></td><td><b>Cliente</b></td><td><b>Concepto</b></td><td>Banco</td><td>ref</td></tr>
              @foreach($pagosC as $pagoC)
               
              <tr>
              <td>{!!$pagoC->fecha_abono!!}</td>
              <td>{!!$pagoC->abono!!} BsF</td>
              <td>{!!$pagoC->forma!!}</td>

              @foreach($cliente as $clientes)
                @foreach($cPago as $cPagos)
                  @if($pagoC->id == $cPagos->id_pago)
                    @if($cPagos->cedula_cliente == $clientes->cedula)
                      <td>{!!$clientes->nombre!!}</td>
                    @endif
                  @endif
                  
                @endforeach
              @endforeach

              <td>{!!$pagoC->concepto!!}</td>
              <td>
                @if($pagoC->forma == "Efectivo")
                  N/A
                @else
                  @if($pagoC->banco != NULL)
                  {!!$pagoC->banco!!}
                  @else 
                  No registrado
                  @endif
                @endif
              </td>
              <td>
                @if($pagoC->forma == "Efectivo")
                  N/A
                @else
                  @if($pagoC->ref != NULL)
                  {!!$pagoC->ref!!}
                  @else 
                  No registrado
                  @endif
                @endif  
              </td>
              <td>
                <form method="GET" action="/recibos/{!!$pagoC->id!!}">
                <button type="submit" class="btn btn-theme"><i class="fa fa-cog"></i>Imprimir recibo</button>
                </form>
                </td>
              
              </tr>
              
              @endforeach
              </table>

                </div>
                <br>
                
                 <div>
                  <form method="GET" action="/pagos/list/all/canceled"><center><button type="submit" class="btn btn-theme">Ver todos</button></center></form>
              <b><h1>Pendientes</h1></b>
              <br>
              <table class="table table-striped table-hover">

              <tr>
              <td><label>Fecha</label></td><td><b>Monto</b></td><td><b>Cliente</b></td><td>Concepto</td></tr>
              @foreach($pagosP as $pagoP)
                
              <tr>
              <td>{!!$pagoP->fecha!!}</td>
              <td>{!!$pagoP->abono!!} BsF</td>

              @foreach($cliente as $clientes)
                @foreach($cPago as $cPagos)
                  @if($pagoP->id == $cPagos->id_pago)
                    @if($cPagos->cedula_cliente == $clientes->cedula)
                      <td>{!!$clientes->nombre!!}</td>
                    @endif
                  @endif
                  
                @endforeach
              @endforeach

              <td>{!!$pagoP->concepto!!}</td>
              <td><form method="GET"  action="/pagos/{!!$pagoP->id!!}">
                <button type="submit" class="btn btn-theme"><i class="fa fa-cog"></i>Cancelar</button>
                </form></td>
              </tr>
              
              @endforeach
              </table>

                </div>
              </div>
            </div>
  

    </section><! --/wrapper -->
      </section><!-- /MAIN CONTENT -->

      <!--main content end-->
      <!--footer start-->
      
      </footer>
      <!--footer end-->
  </section>

    <!-- js placed at the end of the document so the pages load faster -->
    
    <script src="assets/js/jjquery-1.8.3.min.js"></script>
    
    <script src="../../assets/js/jquery.js"></script>
    <script src="../../assets/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="../../assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="../../assets/js/jquery.scrollTo.min.js"></script>
    <script src="../../assets/js/jquery.nicescroll.js" type="text/javascript"></script>


    <!--common script for all pages-->
    <script src="../../assets/js/common-scripts.js"></script>

    <!--script for this page-->
  <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>    
    <script src="../../assets/js/tasks.js" type="text/javascript"></script>

    <script>
      jQuery(document).ready(function() {
          TaskList.initTaskWidget();
      });

      $(function() {
          $( "#sortable" ).sortable();
          $( "#sortable" ).disableSelection();
      });

    </script>
    
    
  <script>
      //custom select box

      $(function(){
          $('select.styled').customSelect();
      });

  </script>