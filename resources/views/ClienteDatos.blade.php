<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title>D'Closet Home Solutions</title>

    <!-- Bootstrap core CSS -->
    <link href="/assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
        
    <!-- Custom styles for this template -->
    <link href="/assets/css/style.css" rel="stylesheet">
    <link href="/assets/css/style-responsive.css" rel="stylesheet">

    <link href="/assets/css/table-responsive.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

  <section id="container" >
      <!-- **********************************************************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      *********************************************************************************************************************************************************** -->
      <!--header start-->
      <header class="header black-bg">
              <div class="sidebar-toggle-box">
                  <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
              </div>
            <!--logo start-->
            <a href="/" class="logo"><b>D'Closet</b></a>
            <!--logo end-->
            <div class="nav notify-row" id="top_menu">
                <!--  notification start -->
                <ul class="nav top-menu">
                    <!-- settings start -->
                    
                    <!-- settings end -->
                    <!-- inbox dropdown start-->
                   
                    <!-- inbox dropdown end -->
                </ul>
                <!--  notification end -->
            </div>
            <div class="top-menu">
                <ul class="nav pull-right top-menu">
                    <li><a class="logout" href="auth/logout">Cerrar sesión</a></li>
                </ul>
            </div>
        </header>
      <!--header end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN SIDEBAR MENU
      *********************************************************************************************************************************************************** -->
      <!--sidebar start-->
      <aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu" id="nav-accordion">
              
                  <p class="centered"><a href="profile.html"><img src="../../assets/images/ui-sam.jpg" class="img-circle" width="60"></a></p>
                  <h5 class="centered">{!!$user=Auth::user()->name!!}</h5>
                    
                  <li class="mt">
                      <a class="sub" href="/">
                          <i class="fa fa-dashboard"></i>
                        <span>Dashboard</span></a>   
                      
                  </li>
                  
                  <li class="sub-menu">
                      <a class="active" href="/clientes">
                          <i class="fa fa-desktop"></i>
                        <span>Clientes</span></a>   
                      
                  </li>
                  
                  <li class="sub-menu">
                      <a class="sub" href="/proyecto">
                          <i class="fa fa-cogs"></i>
                        <span>Proyectos</span></a>   
                      
                  </li>
                  
                  <li class="sub-menu">
                      <a class="sub" href="/citas">
                          <i class="fa fa-book"></i>
                        <span>Citas</span></a>   
                  </li>

                  <li class="sub-menu">
                      <a class="sub" href="/pagos/list/all">
                          <i class="fa fa-book"></i>
                        <span>Gestión de pagos</span></a>   
                  </li>

              </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>
      <!--sidebar end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
          	<h3><i class="fa fa-angle-right"></i> Información del cliente</h3>
		  		<div class="row mt">
			  		<div class="col-lg-12">
                      <div class="content-panel">
                      <h4><i class="fa fa-angle-right"></i> Datos</h4>
                          <section id="unseen">
                            <div id="info">
								<b>Información de cliente</b>
								<br>
								<?php

									$hasDate = false;
									$hasBudget = false;

								?>	
								<div>
									@foreach($data as $cliente)
									<label>Nombre: </label>
									{!!$cliente->nombre!!}
									<br>
									<label>Cédula: </label>
									{!!$cliente->cedula!!}
									<br>
									<label>Telefono: </label>
									{!!$cliente->telefono!!}
									<br>
									<label>Direccion: </label>
									{!!$cliente->direccion!!}
									<br>
									<label>Email: </label>
									{!!$cliente->email!!}
									<br>
									<label>Atendido por: </label>
									{!!$cliente->atendido!!}
									<br>
									<label>Fecha de ingreso</label>
									{!!$cliente->created_at!!}
									<br>
									@endforeach
								</div>
							</div>
							<h4><i class="fa fa-angle-right"></i> Citas de medidas</h4>
							 <section id="no-more-tables">
							  @if($ClienteCita == NULL)
							  <p>El cliente no tiene citas registradas</p>
							  
							  @else
							  <table class="table table-bordered table-striped table-condensed cf">
							  <tr><td><b>Fecha</b></td><td><b>Estado</b></td><td><b>Observaciones</b></td><td><b>Acción</b></td></tr>
							  @foreach($ClienteCita as $clientecita)
								@foreach($cita as $CitaM)
								@if($CitaM->id == $clientecita->id_cita)
                              	
                              	<tr>
                              	<td>{!!$CitaM->fecha!!}</td>
                              	<td>{!!$CitaM->estado!!} 
                              			@if($CitaM->estado == 'Realizada')
										<?php
											$hasDate = true;
										?>
										@endif 
								</td>
								<td>{!!$CitaM->obs!!}</td>
								<td>
									<form method="POST" action="/citaEdit/{!!$cliente->cedula!!}/{!!$CitaM->id!!}">
									<button type="submit" class="btn">Editar cita</button>
									</form>
								</td>
								</tr>
								@endif
                              	@endforeach
                              	@endforeach
                              	
                              </table>
								@endif
								<form method="get" action="/citasM/{!!$cliente->cedula!!}">
    							<button type="submit" class="btn">Nueva Cita</button>
								</form>
                              	<br>
                          	</section>
                          	<h4><i class="fa fa-angle-right"></i> Citas de presupuesto</h4>
							 <section id="no-more-tables">
							  @if($ClientePresupuesto == NULL)
							  <p>El cliente no tiene citas registradas</p>
							  
							  @else
							  <table class="table table-bordered table-striped table-condensed cf">
							  <tr><td><b>Fecha</b></td><td><b>Estado</b></td><td><b>Observaciones</b></td><td><b>Acción</b></td></tr>
							  @foreach($ClientePresupuesto as $CPresupuesto)
								@foreach($Citapresupuesto as $CitaPresupuesto)
								@if($CitaPresupuesto->id == $CPresupuesto->id_cita)
                              	
                              	<tr>
                              	<td>{!!$CitaPresupuesto->fecha!!}</td>
                              	<td>{!!$CitaPresupuesto->estado!!}</td>
								<td>{!!$CitaPresupuesto->obs!!}</td>
								<td>
									<form method="get" action="/citapEdit/{!!$cliente->cedula!!}/{!!$CPresupuesto->id_cita!!}"> 
									<button type="submit" class="btn">Editar cita</button>
									</form>
								</td>
								</tr>
								@endif
                              	@endforeach
                              	@endforeach
                              	
                              </table>
								@endif
								<form method="get" action="/citasP/{!!$cliente->cedula!!}">
    							<button type="submit" class="btn">Nueva Cita</button>
								</form>
								<br>
						<h4><i class="fa fa-angle-right"></i> Proyectos</h4>
						<section id="no-more-tables">
						@if($ClienteProyecto != NULL)
						<table  class="table table-striped table-hover">
						<tr>
						<TD><b>Nombre</b></TD><TD><b>Producto</b></TD><TD><b>Costo</b></TD><TD><b>Observaciones</b></TD><TD><b>Estado</b></TD>
						</tr>
						@foreach($ClienteProyecto as $CProyecto)
						@foreach($Proyecto as $proyectos)
							@if($CProyecto->id_proyecto == $proyectos->id)
							@if($proyectos->estado != "Anulado")
						<tr>
						<TD>
						{!!$proyectos->nombre!!}
						</TD>
						<TD>
						{!!$proyectos->producto!!}
						</TD>
						<TD>
						@if($proyectos->total == 0)
							Sin definir
				
						@else
						{!!$proyectos->total!!} BsF
						@endif
						</TD>
						<TD>
						{!!$proyectos->obs!!}
						</TD>
						<TD>
						{!!$proyectos->estado!!}
						</TD>

						<TD>
						<form method="GET" action="/proyecto/edit/{!!$proyectos->id!!}"><button class="btn" type="submit">Ver</button></form>
						</TD>
				
				@if($proyectos->estado == "Planteado")
					<?php
						$hasBudget = true;
					?>
				@endif
				@endif
				@endif
				</tr>
			@endforeach
			@endforeach
		</table>
		@else
			<p>El cliente no posee ningún proyecto registrado</p>
		@endif
		@if($hasDate == true)
			
					<form method="get" action="/proyecto/nuevo/{!!$cliente->cedula!!}">
    	    		<button type="submit" class="btn">Nuevo Proyecto</button>
		    		</form>
		    		@if($hasBudget)
		    		<br>
		    		<form method="get" action="/presupuesto/{!!$cliente->cedula!!}">
    	    		<button type="submit" class="btn">Generar presupuesto</button>
		    		</form>
		    		<br>
					@endif
		 @endif
                              
                          	</section>
         <h4><i class="fa fa-angle-right"></i> Pagos realizados</h4>
		<section id="no-more-tables">
		@if($clientePago!=NULL)	
		<table class="table table-striped table-hover">
			<tr><TD><b>Fecha</b></TD><TD><b>Monto</b></TD><TD><b>Forma</b></TD><TD><b>Concepto</b></TD></tr>
			@foreach($pago as $pagos)
			@foreach($clientePago as $cPago)
			@if($cPago->id_pago == $pagos->id)
			@if($pagos->estado == "cancelado")
			<tr>	
				<TD>{!!$pagos->fecha_abono!!}</TD>
				<TD>{!!$pagos->abono!!} BsF</TD>		
				<TD>{!!$pagos->forma!!}</TD>
				<TD>{!!$pagos->concepto!!}</TD>				
			</tr>
			@endif
			@endif
			@endforeach
			@endforeach
		</table>
		@else
			<p>El cliente no posee pagos registrados hasta los momentos</p>
		@endif	
	</section>
	<h4><i class="fa fa-angle-right"></i> Pagos pendientes</h4>
		<section id="no-more-tables">
		@if($clientePago != NULL)	
		<table class="table table-striped table-hover">
			<tr><TD><b>Fecha</b></TD><TD><b>Monto</b></TD><TD><b>Concepto</b></TD><TD><b>Acción</b></TD></tr>
			@foreach($clientePago as $cPago)
			@foreach($pago as $pagos)

			
			@if($cPago->id_pago == $pagos->id)
			@if($pagos->estado == "pendiente")
			<tr>	
				<TD>{!!$pagos->fecha!!}</TD>
				<TD>{!!$pagos->abono!!} BsF</TD>
				<TD>{!!$pagos->concepto!!}</TD>		
				<TD><form methos="GET" action="/pagos/{!!$pagos->id!!}"><button class="btn" type="submit">Registrar pago</button></form></TD>				
			
			</tr>
			@endif
			@endif
			@endforeach
			@endforeach
		</table>
		@else
			<p>El cliente no posee pagos pendientes</p>
		@endif	
		</section>
                          </section>
                      </div><!-- /content-panel -->
                  </div><!-- /col-lg-12 -->
              </div><!-- /row -->

		</section><! --/wrapper -->
      </section><!-- /MAIN CONTENT -->

      <!--main content end-->
      <!--footer start-->
      <footer class="site-footer">
          <div class="text-center">
              2016 - Cumaná
              <a href="responsive_table.html#" class="go-top">
                  <i class="fa fa-angle-up"></i>
              </a>
          </div>
      </footer>
      <!--footer end-->
  </section>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="/assets/js/jquery.js"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="/assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="/assets/js/jquery.scrollTo.min.js"></script>
    <script src="/assets/js/jquery.nicescroll.js" type="text/javascript"></script>


    <!--common script for all pages-->
    <script src="assets/js/common-scripts.js"></script>

    <!--script for this page-->
    

  </body>
</html>


