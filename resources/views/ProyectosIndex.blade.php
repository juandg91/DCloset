
<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title>D'Closet Home solutions - clientes</title>

    <!-- Bootstrap core CSS -->
    <link href="/assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    
    <!-- Custom styles for this template -->
    <link href="/assets/css/style.css" rel="stylesheet">
    <link href="/assets/css/style-responsive.css" rel="stylesheet">
    <link rel="/stylesheet" href="assets/css/to-do.css">    



<section id="container" >
      <!-- **********************************************************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      *********************************************************************************************************************************************************** -->
      <!--header start-->
      <header class="header black-bg">
              <div class="sidebar-toggle-box">
                  <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
              </div>
            <!--logo start-->
            <a href="/" class="logo"><b>D'Closet</b></a>
            <!--logo end-->
            
            <div class="top-menu">
                <ul class="nav pull-right top-menu">
                    <li><a class="logout" href="auth/logout">Cerrar sesión</a></li>
                </ul>
            </div>

        </header>
      <!--header end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN SIDEBAR MENU
      *********************************************************************************************************************************************************** -->
      <!--sidebar start-->
     <aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu" id="nav-accordion">
              
                  <p class="centered"><a href="profile.html"><img src="/assets/images/ui-sam.jpg" class="img-circle" width="60"></a></p>
                  <h5 class="centered">{!!$user=Auth::user()->name!!}</h5>
                    
                  <li class="mt">
                      <a class="sub" href="/">
                          <i class="fa fa-dashboard"></i>
                        <span>Dashboard</span></a>   
                      
                  </li>
                  
                  <li class="sub-menu">
                      <a class="sub" href="/clientes">
                          <i class="fa fa-desktop"></i>
                        <span>Clientes</span></a>   
                      
                  </li>
                  
                  <li class="sub-menu">
                      <a class="active" href="/proyecto">
                          <i class="fa fa-cogs"></i>
                        <span>Proyectos</span></a>   
                      
                  </li>
                  
                  <li class="sub-menu">
                      <a class="sub" href="/citas">
                          <i class="fa fa-book"></i>
                        <span>Citas</span></a>   
                  </li>
              
                  <li class="sub-menu">
                      <a class="sub" href="/pagos/list/all">
                          <i class="fa fa-book"></i>
                        <span>Gestión de pagos</span></a>   
                  </li>

              </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>
      <!--sidebar end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">      	
          	<!-- SIMPLE TO DO LIST -->
          	<div class="row mt">
          		<div class="col-md-12">
          			<div class="white-panel pn">

          				<table class="table table-striped table-hover">
<tr><center><h1>Proyectos Propuestos</h1></center></tr>	
<tr><td>ID</td><td>Nombre</td><td>Linea</td><td>Producto</td><td>Cliente</td></tr>
	@foreach($proyectos as $project)
	
	<tr>

	@if($project->estado == "Planteado")
		<td>{!!$project->id!!}</td>
		<td>{!!$project->nombre!!}</td>
		<td>{!!$project->linea!!}</td>
		<td>{!!$project->producto!!}</td>
		@foreach($inter as $Inter)
				@if($Inter->id_proyecto == $project->id)
					
				@foreach($clientes as $client)

					@if($Inter->cedula_cliente == $client->cedula)
						<td>{!!$client->nombre!!}</td>
					@endif

				@endforeach


				@endif

		@endforeach
		
		<form method="GET" action="/proyecto/edit/{!!$project->id!!}">
			<td><button type="submit" class="btn">Editar</button><td>
		</form>
		<br>
		@endif
	</tr>
	@endforeach
</table>
<table class="table table-striped table-hover">
<tr><center><h1>Proyectos cerrados</h1></center></tr>
<tr><td>ID</td><td>Nombre</td><td>Linea</td><td>Producto</td><td>Estado</td><td>Cliente</td></tr>
<tr>
	@foreach($proyectos as $project)

	@if($project->estado != "Planteado")
	   <td>{!!$project->id!!}</td>
		
		<td>{!!$project->nombre!!}</td>

		<td>{!!$project->linea!!}</td>

		<td>{!!$project->producto!!}</td>

    <td>{!!$project->estado!!}</td>

		@foreach($inter as $Inter)
				@if($Inter->id_proyecto == $project->id)
					
				@foreach($clientes as $client)

					@if($Inter->cedula_cliente == $client->cedula)
						<td>{!!$client->nombre!!}</td>
					@endif

				@endforeach


				@endif
		@endforeach

<!--		@foreach($budget as $contrato)
			@if($contrato->id_proyecto == $project->id)
			<form method="GET" action="pdf/{!!$contrato->id_presupuesto!!}">
			<button type="submit" class="btn">Ver contrato</button>
			</form>  
			@endif

		@endforeach!-->

		<form method="GET" action="/proyecto/edit/{!!$project->id!!}">
		<td>	<button type="submit" class="btn">Ver</button> </td>
		</form>

	@endif
</tr>
	@endforeach
</table>
	</div>

          			</div>
          		</div>
          	</div>
	

		</section><! --/wrapper -->
      </section><!-- /MAIN CONTENT -->

      <!--main content end-->
      <!--footer start-->
      
      </footer>
      <!--footer end-->
  </section>
<footer class="site-footer">
          <div class="text-center">
              2016 - Cumaná
                  <i class="fa fa-angle-up"></i>
              </a>
          </div>
    <!-- js placed at the end of the document so the pages load faster -->
    <script src="/assets/js/jquery.js"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="/assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="/assets/js/jquery.scrollTo.min.js"></script>
    <script src="/assets/js/jquery.nicescroll.js" type="text/javascript"></script>


    <!--common script for all pages-->
    <script src="../assets/js/common-scripts.js"></script>

    <!--script for this page-->
	<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>    
    <script src="../assets/js/tasks.js" type="text/javascript"></script>

    <script>
      jQuery(document).ready(function() {
          TaskList.initTaskWidget();
      });

      $(function() {
          $( "#sortable" ).sortable();
          $( "#sortable" ).disableSelection();
      });

    </script>
    
    
  <script>
      //custom select box

      $(function(){
          $('select.styled').customSelect();
      });

  </script>



