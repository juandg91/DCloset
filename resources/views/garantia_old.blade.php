<script type="text/javascript">

var BASEURL = '{{ url() }}/';
alert(BASEURL);
</script>


<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title>D'Closet Home solutions - clientes</title>

    <!-- Bootstrap core CSS -->
    <link href="../assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="../assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    
    <!-- Custom styles for this template -->
    <link href="../assets/css/style.css" rel="stylesheet">
    <link href="../assets/css/style-responsive.css" rel="stylesheet">
    <link rel="../stylesheet" href="assets/css/to-do.css">    



<section id="container" >
      <!-- **********************************************************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      **************************************************************************************************************************************************s********* -->
      <!--header start-->
      <header class="header black-bg">
              <div class="sidebar-toggle-box">
                  <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
              </div>
            <!--logo stsart-->
            <a href="/" class="logo"><b>D'Closet</b></a>
            <!--logo end-->
            
            <div class="top-menu">
                <ul class="nav pull-right top-menu">
                    <li><a class="logout" href="auth/logout">Cerrar sesión</a></li>
                </ul>
            </div>

        </header>
      <!--header end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN SIDEBAR MENU
      *********************************************************************************************************************************************************** -->
      <!--sidebar start-->
     <aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu" id="nav-accordion">
              
                  <p class="centered"><a href="profile.html"><img src="../assets/images/ui-sam.jpg" class="img-circle" width="60"></a></p>
                  <h5 class="centered">{!!$user=Auth::user()->name!!}</h5>
                    
                  <li class="mt">
                      <a class="active" href="/">
                          <i class="fa fa-dashboard"></i>
                          <span>Dashboard</span>
                      </a>
                  </li>

                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-desktop"></i>
                          <span>Clientes</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="../clientes">General</a></li>
                          <li><a  href="../clientes/trafico">Tráfico</a></li>
                          <li><a  href="../registro">Registrar nuevo</a></li> 
                      </ul>
                  </li>

                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-cogs"></i>
                          <span>Proyectos</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="../proyecto">General</a></li>
                          <li><a  href="proyecto/nuevo">Nuevo</a></li>
                        <!--  <li><a  href="todo_list.html">Todo List</a></li> !-->
                      </ul>
                  </li>
                  
                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-book"></i>
                          <span>Control de citas</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="../citasM">Nueva cita de medidas</a></li>
                          <li><a  href="../citasShow">Ver citas de medidas</a></li>
                          <li><a  href="blank.html">Nueva cita de presupuesto</a></li>
                          <li><a  href="../CitasPShow">Ver citas de presupuesto</a></li>
                          
                      </ul>
                  </li>
                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-tasks"></i>
                          <span>Gestión de pagos</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="/pagos/list/all">Administrar pagos</a></li>
                      </ul>
                  </li>

              </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>
      <!--sidebar end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">      	
          	<!-- SIMPLE TO DO LIST -->
          	<div class="row mt">
          		<div class="col-md-12">
          			<div class="white-panel pn">
			<center><h1>Registro de proyectos anteriores en garantía</h1></center>

              <div class="row mt">
              <div class="col-lg-12">
                  <div class="form-panel">
                      <h4 class="mb"><i class="fa fa-angle-right"></i> Datos</h4>
                      <form method="POST" name="create_post" class="form-horizontal style-form">

                  <div class="form-group">
                      <label class="col-sm-2 col-sm-2 control-label">Nombre del cliente</label> 
                      <div class="col-sm-10">
                        <input type="text" name="cliente" id="cliente" class="form-control round-form" placeholder="Nombre del cliente" required>
                      </div>
                  </div>
                  <div class="form-group">
                      <label class="col-sm-2 col-sm-2 control-label">Número de contrato</label> 
                      <div class="col-sm-10">
                        <input type="text" name="numero_contrato" id="numero_contrato" class="form-control round-form" placeholder="Identificación del contrato" required>
                      </div>
                  </div>
                  <div class="form-group">
                      <label class="col-sm-2 col-sm-2 control-label">Producto</label> 
                      <div class="col-sm-10">
                        <input type="text" name="producto" id="producto" class="form-control round-form" placeholder="Tipo de producto" required>
                      </div>
                  </div>
                  <div class="form-group">
                      <label class="col-sm-2 col-sm-2 control-label">Fecha de instalación</label> 
                      <div class="col-sm-10">
                        <input type="date" name="fecha" id="fecha" class="form-control round-form" placeholder="Fecha de culminación de la instalación" required>
                      </div>
                  </div>
                <div class="row mt">
              <div class="col-lg-12">
                <label class="col-sm-2 col-sm-2 control-label">Tiempo de garantía</label>
                  <label class="checkbox-inline">
                  <input type="checkbox" id="inlineCheckbox1 checkbox" name="6meses" value="6 meses"> 6 meses
                </label>
                <label class="checkbox-inline">
                <input type="checkbox" id="inlineCheckbox2 checkbox" name="6meses" value="1 año"> 1 año
                </label>
            <div id="msj-error" class="alert alert-warning" role="alert" style="display:none">
                 <strong>Error: contrato ya registrado</strong>
            </div>
           <div id="msj-success" class="alert alert-success alert-dismissible" role="alert" style="display:none">
                <strong>Registro almacenado correctamente</strong>
           </div>   
           </div>

         </div>
                <button type="submit" class="btn btn-theme">Guardar</button>
          			</div></form>
          		</div>
          	</div>
	

		</section><! --/wrapper -->
      </section><!-- /MAIN CONTENT -->

      <!--main content end-->
      <!--footer start-->
      
      </footer>
      <!--footer end-->
  </section>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="../assets/js/jquery.js"></script>
    <script src="../assets/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="../assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="../assets/js/jquery.scrollTo.min.js"></script>
    <script src="../assets/js/jquery.nicescroll.js" type="text/javascript"></script>


    <!--common script for all pages-->
    <script src="../assets/js/common-scripts.js"></script>

    <!--script for this page-->
	<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>    
    <script src="/assets/js/tasks.js" type="text/javascript"></script>

    <script>
      jQuery(document).ready(function() {
          TaskList.initTaskWidget();
          
      });
      var asd = "hola";
      var BASEURL = {!!url()!!}/;
      alert('hola');
      $(function() {
          $( "#sortable" ).sortable();
          $( "#sortable" ).disableSelection();
      });

    </script>
    
  <script src="/assets/js/warranty.js" type="text/javascript"></script>  
  <script>
      //custom select box

      $(function(){
          $('select.styled').customSelect();
      });

  </script>