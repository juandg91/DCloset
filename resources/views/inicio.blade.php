<script type="text/javascript">

var BASEURL = '{{ url() }}/';
var cedula_aux = null;

</script>
<?php
  $ci = "<script>document.write(cedula_aux)</script>"
?>
<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title>D'Closet Home solutions - clientes</title>

    <!-- Bootstrap core CSS -->
    <link href="/assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="assets/js/bootstrap-datepicker/css/datepicker.css" />
    <link rel="stylesheet" type="text/css" href="assets/js/bootstrap-daterangepicker/daterangepicker.css" />
    
    <!-- Custom styles for this template -->
    <link href="/assets/css/style.css" rel="stylesheet">
    <link href="/assets/css/style-responsive.css" rel="stylesheet">
  



<section id="container" >
      <!-- **********************************************************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      **************************************************************************************************************************************************s********* -->
      <!--header start-->
      <header class="header black-bg">
              <div class="sidebar-toggle-box">
                  <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
              </div>
            <!--logo stsart-->
            <a href="/" class="logo"><b>D'Closet</b></a>
            <!--logo end-->
            
            <div class="top-menu">
                <ul class="nav pull-right top-menu">
                    <li><a class="logout" href="auth/logout">Cerrar sesión</a></li>
                </ul>
            </div>

        </header>
      <!--header end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN SIDEBAR MENU
      *********************************************************************************************************************************************************** -->
      <!--sidebar start-->
     <aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu" id="nav-accordion">
              
                  <p class="centered"><a href="profile.html"><img src="/assets/images/ui-sam.jpg" class="img-circle" width="60"></a></p>
                  <h5 class="centered">{!!$user=Auth::user()->name!!}</h5>
                    
                  <li class="mt">
                      <a class="active" href="/">
                          <i class="fa fa-dashboard"></i>
                          <span>Dashboard</span>
                      </a>
                  </li>

                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-desktop"></i>
                          <span>Clientes</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="/clientes">General</a></li>
                          <li><a  href="/clientes/trafico">Tráfico</a></li>
                          <li><a  href="/registro">Registrar nuevo</a></li> 
                      </ul>
                  </li>

                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-cogs"></i>
                          <span>Proyectos</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="/proyecto">General</a></li>
                          <li><a  href="proyecto/nuevo">Nuevo</a></li>
                        <!--  <li><a  href="todo_list.html">Todo List</a></li> !-->
                      </ul>
                  </li>
                  
                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-book"></i>
                          <span>Control de citas</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="/citasM">Nueva cita de medidas</a></li>
                          <li><a  href="/citasShow">Ver citas de medidas</a></li>
                          <li><a  href="blank.html">Nueva cita de presupuesto</a></li>
                          <li><a  href="/CitasPShow">Ver citas de presupuesto</a></li>
                          
                      </ul>
                  </li>
                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-tasks"></i>
                          <span>Gestión de pagos</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="/pagos/list/all">Administrar pagos</a></li>
                      </ul>
                  </li>

              </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>

<script type="text/javascript">
  
  document.getElementById("canal-otro").style.visibility = 'hidden';
  
  function canales(obj)
  {

    canalDiv = document.getElementById("canal-otro");

    if(obj.value == "Otro")
    {
      
      canalDiv.style.visibility = 'visible';
    }
    if(obj.value == "Vitrina")
    {
      
      canalDiv.style.visibility = 'hidden';
    }
    if(obj.value == "Radio")
    {
      
      canalDiv.style.visibility = 'hidden';
    }
  }

</script>

 <section id="main-content">
          <section class="wrapper">       
            <!-- SIMPLE TO DO LIST -->
            <div class="row mt">
              <div class="col-md-12">
                <div class="form-panel">
    
<!--       <img src="images/dcloset logo.png" class="img-rounded img-responsive"> -->
      
        <center><H1>Registro de clientes</H1></center>

        
          <form name="registro" class="form-horizontal style-form" method="POST">
            <div class = "form-group" >
            <label class="col-sm-2 col-sm-2 control-label">Nombre</label> 
            <div class="col-sm-10">
            <input type="text" name="nombre" id="nombre" class="form-control" placeholder="Nombre del cliente" required>
            </div>
            </div>
            <div class = "form-group" >
            <label class="col-sm-2 col-sm-2 control-label">Cédula</label>
            <div class="col-sm-10"> 
            <input type="text" name="cedula" id="cedula" class="form-control" placeholder="Cedula del cliente" required>
            </div>
            </div>
            <div class = "form-group">
            <label class="col-sm-2 col-sm-2 control-label">Dirección</label>
            <div class="col-sm-10"> 
            <input type="text" name="direccion" id="direccion" class="form-control" placeholder="Dirección del cliente" required>
            </div >
            </div>
            <div class="form-group">
            <label class="col-sm-2 col-sm-2 control-label">Telefono</label>
            <div class="col-sm-10"> 
            <input type="text" name="telefono" id="telefono" class="form-control" placeholder="Número telefonico del cliente" required>
            </div>
            </div>
            <div class="form-group">
            <label class="col-sm-2 col-sm-2 control-label">Email</label> 
            <div class="col-sm-10">
            <input type="email" name="email" id="email" class="form-control" placeholder="Correo electrónico" required>
            </div>
            </div>
            <div class="form-group">
            <label class="col-sm-2 col-sm-2 control-label">Atendido por</label>
            <div class="col-sm-10"> 
            <input type="text" name="asistente" id="asistente" class="form-control" placeholder="Persona que atendió al cliente" required>
            </div>
            </div>
            <div class="form-group">
            <label class="col-sm-2 col-sm-2 control-label">Canal</label>
            <div class="col-sm-10">
            <select class="form-control" name="canal" id="canal" onchange="return canales(this)" required>
                 <option value="Vitrina">Vitrina</option>
                 <option value="Radio">Radio</option>
                 <option value="Otro">Otro</option>
            </select>
          </div>
          </div>
          <div class="form-group" id="canalDiv" style="visibility:hidden;">
            <label class="col-sm-2 col-sm-2 control-label">Canal</label>
            <div class="col-sm-10">
            <input type="text" name="canal-otro" id="canal-otro" class="form-control" placeholder="Cual otro">
          </div>
        </div>
        
            <!-- Button trigger modal -->
            <button type="submit" class="btn btn-theme">
              Aceptar
            </button>  
          </form>


          <!-- Modal -->
           <div class="modal fade" id="getCodeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
             <div class="modal-dialog modal-lg">
                <div class="modal-content">
                 <div class="modal-header">
                   <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                   <h4 class="modal-title" id="myModalLabel"> Error en registro de cliente </h4>
                 </div>
                 <div class="modal-body" id="getCode" style="overflow-x: scroll;">
                    Imposible registrar cliente
                 </div>
                 <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Aceptar</button>
                  </div>
              </div>
             </div>
           </div> 

           <!-- Modal -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Registro exitoso</h4>
                  </div>
                  <div class="modal-body">
                    ¿Desea asignar una cita de medidas para el cliente?
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                    <form method="GET" action='<?php echo "/citasM/".$ced = $ci ?>'>
                    <button type="submit" class="btn btn-primary">Si</button></form>
                  </div>
                </div>
              </div>
            </div>   
        
     </div> 
    </div>
  </div>
</section>
</section>
    <script src="/assets/js/jquery.js"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="/assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="/assets/js/jquery.scrollTo.min.js"></script>
    <script src="/assets/js/jquery.nicescroll.js" type="text/javascript"></script>


    <!--common script for all pages-->
    <script src="/assets/js/common-scripts.js"></script>

    <!--script for this page-->
  <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>    
    <script src="/assets/js/tasks.js" type="text/javascript"></script>
    <script src="assets/js/form-component.js"></script>   

    <script>
      jQuery(document).ready(function() {
          TaskList.initTaskWidget();
      });
      
      $(function() {
          $( "#sortable" ).sortable();
          $( "#sortable" ).disableSelection();
      });
      
    </script>
    <script src="assets/js/cliente_registro.js" type="text/javascript"></script>
    
    
  <script>
      //custom select box

      $(function(){
          $('select.styled').customSelect();
      });

  </script>