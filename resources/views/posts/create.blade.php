@extends('layouts.master')

@section('content')


<form method="POST" action="posts/create">
	<div class="form group">
		<label for="title">CI </label>
		<input type="text" name="cedula" class="form-control" id="cedula" placeholder="Cedula del asistente">
	</div>
	<div class="form group">
		<label for="password">Password </label>
		<input type="password" name="password" class="form-control" id="password" placeholder="Password of assistant member">
	</div>
	<div class="form group">
		<label for="nombre">Nombre </label>
		<input type="text" name="nombre" class="form-control" id="nombre" placeholder="Name of assistant member">
	</div>
	<input type="hidden" name="token" value="{{ csrf_token() }}">
	<button type="submit" class="btn btn-default">Submit</button>
	</form>
@stop