
@extends('login')


    <title>DCloset - Inicio de sesión</title>





      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->

      <div id="login-page">

        <div class="container">
        
                <form class="form-horizontal" method="POST">
                {!! csrf_field() !!}
                <h2 class="form-login-heading">Inicio de Sesión</h2>
                <div class="login-wrap">
                    <input type="text" name="email" class="form-control" placeholder="Email" autofocus>
                    <br>
                    <input type="password" name="password" class="form-control" placeholder="Contraseña">
                    <label class="checkbox">
                        <span class="pull-right">
                            <a data-toggle="modal" href="login.html#myModal"> Olvidaste tu contraseña?</a>
        
                        </span>
                    </label>
                    <button class="btn btn-theme btn-block" href="index.html" type="submit"><i class="fa fa-lock"></i> Iniciar</button>
                    <hr>
                    
                
                    <div class="registration">
                        Recordarme<br/>
                        <input type="checkbox" name="remember"> 
                    </div>
                    </form>  
        
                </div>
        
                  <!-- Modal -->
                  <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
                      <div class="modal-dialog">
                          <div class="modal-content">
                              <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                  <h4 class="modal-title">Forgot Password ?</h4>
                              </div>
                              <div class="modal-body">
                                  <p>Enter your e-mail address below to reset your password.</p>
                                  <input type="text" name="email" placeholder="Email" autocomplete="off" class="form-control placeholder-no-fix">
        
                              </div>
                              <div class="modal-footer">
                                  <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                                  <button class="btn btn-theme" type="button">Submit</button>
                              </div>
                          </div>
                      </div>
                  </div>
                  <!-- modal -->
              
        
        </div>
      </div>
  
