


<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title>D'Closet Home solutions - clientes</title>

    <!-- Bootstrap core CSS -->
    <link href="/assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    
    <!-- Custom styles for this template -->
    <link href="/assets/css/style.css" rel="stylesheet">
    <link href="/assets/css/style-responsive.css" rel="stylesheet">
    <link rel="stylesheet" href="/assets/css/to-do.css">    



<section id="container" >
      <!-- **********************************************************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      *********************************************************************************************************************************************************** -->
      <!--header start-->
      <header class="header black-bg">
              <div class="sidebar-toggle-box">
                  <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
              </div>
            <!--logo start-->
            <a href="/" class="logo"><b>D'Closet</b></a>
            <!--logo end-->
            
            <div class="top-menu">
                <ul class="nav pull-right top-menu">
                    <li><a class="logout" href="auth/logout">Cerrar sesión</a></li>
                </ul>
            </div>

        </header>
      <!--header end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN SIDEBAR MENU
      *********************************************************************************************************************************************************** -->
      <!--sidebar start-->
     <aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu" id="nav-accordion">
              
                  <p class="centered"><a href="profile.html"><img src="/assets/images/ui-sam.jpg" class="img-circle" width="60"></a></p>
                  <h5 class="centered">{!!$user=Auth::user()->name!!}</h5>
                    
                  <li class="mt">
                      <a class="sub" href="/">
                          <i class="fa fa-dashboard"></i>
                        <span>Dashboard</span></a>   
                      
                  </li>
                  
                  <li class="sub-menu">
                      <a class="active" href="/clientes">
                          <i class="fa fa-desktop"></i>
                        <span>Clientes</span></a>   
                      
                  </li>
                  
                  <li class="sub-menu">
                      <a class="sub" href="/proyecto">
                          <i class="fa fa-cogs"></i>
                        <span>Proyectos</span></a>   
                      
                  </li>
                  
                  <li class="sub-menu">
                      <a class="sub" href="/citas">
                          <i class="fa fa-book"></i>
                        <span>Citas</span></a>   
                  </li>
                  <li class="sub-menu">
                      <a class="sub" href="/pagos/list/all">
                          <i class="fa fa-book"></i>
                        <span>Gestión de pagos</span></a>   
                  </li>
              </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>
      <!--sidebar end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">      	
          	<!-- SIMPLE TO DO LIST -->
          	<div class="row mt">
          		<div class="col-md-12">
          			<div class="white-panel pn">

          					<h1><center>Clientes</h1></center>


					<FORM class="form-inline" method="POST" role="form" action="/clientes/find">
					<div class="form-group">
						<div class="col-sm-4">
						<input type="text" name="token" class="form-control" placeholder="buscar cliente">
						</div>
					</div>
					<button type="submit" class="btn btn-theme" placeholder="cédula o nombre">Buscar</button>

					
					</FORM>

					<h2>Últimos clientes</h2>
					<div class="table-responsive">
						<TABLE class="table table-striped table-hover">
						<TR><TD>NOMBRE</TD><TD>FECHA</TD><TD>TELEFONO</TD><TR>
						@foreach($clientes as $cliente)
						<TR>
						<TD>
							{!!$cliente->nombre!!}
						</TD>
						<TD>
							{!!$cliente->created_at!!}
						</TD>
						<TD>
							{!!$cliente->telefono!!}
						</TD>
						<TD><FORM method="GET" action="/clientes/ver/{!!$cliente->cedula!!}"><button type="submit" class="btn btn-default"  name="ver">Ver</button></FORM></TD>
						</TR>
						@endforeach
						</TABLE>
					</div>


					<FORM class="form-inline" method="GET" role="form" action="/clientes/trafico">
				
					<button type="submit" class="btn btn-default">Tráfico de clientes</button>
					</FORM>	


          			</div>
          		</div>
          	</div>
	

		</section><! --/wrapper -->
      </section><!-- /MAIN CONTENT -->

      <!--main content end-->
      <!--footer start-->
      
      </footer>
      <!--footer end-->
  </section>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="/assets/js/jquery.js"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="/assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="/assets/js/jquery.scrollTo.min.js"></script>
    <script src="/assets/js/jquery.nicescroll.js" type="text/javascript"></script>


    <!--common script for all pages-->
    <script src="/assets/js/common-scripts.js"></script>

    <!--script for this page-->
	<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>    
    <script src="/assets/js/tasks.js" type="text/javascript"></script>

    <script>
      jQuery(document).ready(function() {
          TaskList.initTaskWidget();
      });

      $(function() {
          $( "#sortable" ).sortable();
          $( "#sortable" ).disableSelection();
      });

    </script>
    
    
  <script>
      //custom select box

      $(function(){
          $('select.styled').customSelect();
      });

  </script>


