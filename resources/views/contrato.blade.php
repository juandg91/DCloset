    <title>Contrato</title>
<!--    <link rel="stylesheet" href="assets/css/contratoStyle.css" media="all" />  !-->
    <img src="assets/images/dclosetlogo.png">
    <address contenteditable>
        <p>D'Closet, C.A</p>
        <p>RIF: J-40459192-3<br>Cumaná</p>
        <p>Av. Gran Mariscal frente a plaza Martí</p>
      </address>
    
    Fecha de elaboración: <?php setlocale (LC_TIME, "es_ES"); echo date('l jS \de F Y'); ?>
 <div>   
<center><b>Datos del cliente</b></center><br>

  @foreach($cliente as $clienteData)
        <table>
        <tr>   
        <td><b>Nombre y apellidos</b></td><td>{!!$clienteData->nombre!!}</td>
        </tr>
        <tr>
        <td><b>C.I / RIF</b></td> <td>V-{!!$clienteData->cedula!!}</td>
        </tr>
        <tr><td><b>Dirección</b></td><td>{!!$clienteData->direccion!!}</td></tr>
        <tr><td><b>Estado</b></td> <td>Sucre</td></tr>
        <tr><td><b>Telefono de contacto</b></td> <td>{!!$clienteData->telefono!!}</td></tr>
        <tr><td><b>Contrato Número</b></td> <td>1</td></tr>
        </table>
  @endforeach 
<div>
<br>
<center><b>Datos del producto adquirido</b></center>

@foreach($projects as $arrayP)

  @foreach($arrayP as $get)
      <table>
      <tr><td><b>Producto</b></td>
      
      <td>{!!$get->producto!!}</td>
      </tr>
      <tr><td><b>Modelo</b></td></tr>
      @if($get->producto != "vestier")
        <tr><td><b>Tipo/Apertura: </b></td><td>{!!$get->puerta!!}</td></tr>
      @endif  
      <tr><td><b>Manillas: </td><td>{!!$get->manillas!!}</td></tr>
      
      <tr><td><b>Color</td><td>{!!$get->color!!}</td></tr>
      
      
      @if($get->producto != "vestier")
        <tr><td>Acabado de puertas: </td></tr>
      @endif
      @if($get->producto == "cocina")  
        <tr><td>Estructura interna</td><td>{!!$get->estructura!!}</td></tr>
        <tr><td>Tope</td></tr>
      @endif  
      <tr><td>Color módulo</td></tr>
    </table>
  @endforeach

@endforeach

<center><b>Precio de venta</b></center>
<table>
@foreach($price as $mount)
  
    <tr><td><b>Subtotal</b></td><td>{!!$mount->subtotal!!} BsF</td></tr>
    <tr><td><b>Descuento</b></td><td>{!!$mount->descuento!!} %</td></tr>
    <tr><td><b>Impuesto</b></td><td>{!!$mount->impuesto!!} BsF</td></tr>
    <tr><td><b>Total</b></td><td>{!!$mount->total!!} BsF</td></tr>
@endforeach
</table>
<center><b>Condiciones pago del contrato</b></center>

@foreach($price as $condition)
  <table>
  <tr><td><b>Reserva</b></td><td>0</td></tr>
  
  @if($condition->cuotas > 1)
    <tr><td><b>Inicial</b></td><td>{!!$condition->inicial!!} BsF</td></tr>
    <tr><td><b>Segundo pago</b></td><td>{!!$condition->inicial!!} BsF</td></tr>
    <tr><td><b>Monto Final</b></td><td>{!!$condition->total!!} BsF</td></tr>
  @endif
  @if($condition->cuotas == 1)
    <tr><td><b>Número de cuotas</b></td><td>Pago único</td></tr>
    <tr><td><b>Monto</b></td><td>{!!$condition->total!!} BsF</td></tr>
  @endif    
  <tr><td><b>Total a cancelar antes de la entrega</b></td></tr>  
  <tr><td><b>Monto financiado</b></td><td>{!!$condition->inicial!!} BsF</td></tr>  
  <tr><td><b>Total a cancelar al instalar</b></td></tr>
  </table>
@endforeach  
    <footer>
      <br>
      <br>
      <br>
      <center>_______________________</center>      _____________________________          <br>             
        <center>Firma del comprador</center>            Firma del asesor 
    </footer>