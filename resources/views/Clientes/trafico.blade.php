<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title>D'Closet Home solutions - clientes</title>

    <!-- Bootstrap core CSS -->
    <link href="../assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="../assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    
    <!-- Custom styles for this template -->
    <link href="../assets/css/style.css" rel="stylesheet">
    <link href="../assets/css/style-responsive.css" rel="stylesheet">
    <link rel="../stylesheet" href="assets/css/to-do.css">    



<section id="container" >
      <!-- **********************************************************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      **************************************************************************************************************************************************s********* -->
      <!--header start-->
      <header class="header black-bg">
              <div class="sidebar-toggle-box">
                  <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
              </div>
            <!--logo stsart-->
            <a href="/" class="logo"><b>D'Closet</b></a>
            <!--logo end-->
            
            <div class="top-menu">
                <ul class="nav pull-right top-menu">
                    <li><a class="logout" href="auth/logout">Cerrar sesión</a></li>
                </ul>
            </div>

        </header>
      <!--header end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN SIDEBAR MENU
      *********************************************************************************************************************************************************** -->
      <!--sidebar start-->
     <aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu" id="nav-accordion">
              
                  <p class="centered"><a href="profile.html"><img src="../assets/images/ui-sam.jpg" class="img-circle" width="60"></a></p>
                  <h5 class="centered">{!!$user=Auth::user()->name!!}</h5>
                    
                  <li class="mt">
                      <a class="active" href="/">
                          <i class="fa fa-dashboard"></i>
                          <span>Dashboard</span>
                      </a>
                  </li>

                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-desktop"></i>
                          <span>Clientes</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="../clientes">General</a></li>
                          <li><a  href="../clientes/trafico">Tráfico</a></li>
                          <li><a  href="../registro">Registrar nuevo</a></li> 
                      </ul>
                  </li>

                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-cogs"></i>
                          <span>Proyectos</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="../proyecto">General</a></li>
                          <li><a  href="proyecto/nuevo">Nuevo</a></li>
                        <!--  <li><a  href="todo_list.html">Todo List</a></li> !-->
                      </ul>
                  </li>
                  
                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-book"></i>
                          <span>Control de citas</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="../citasM">Nueva cita de medidas</a></li>
                          <li><a  href="../citasShow">Ver citas de medidas</a></li>
                          <li><a  href="blank.html">Nueva cita de presupuesto</a></li>
                          <li><a  href="../CitasPShow">Ver citas de presupuesto</a></li>
                          
                      </ul>
                  </li>
                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-tasks"></i>
                          <span>Gestión de pagos</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="/pagos/list/all">Administrar pagos</a></li>
                      </ul>
                  </li>

              </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>
      <!--sidebar end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">      	
          	<!-- SIMPLE TO DO LIST -->
          	<div class="row mt">
          		<div class="col-md-12">
          			<div class="white-panel pn">

          					<?php
                    
								$cont = 0;
							?>
			<center><h1>Tráfico de clientes<h1></center>

			<div class="table-responsive">
			<table class="table table-striped table-hover">
			<tr> <td><strong>Fecha</strong></td> <td><strong>Nombre</strong></td>  <td><strong>Cedula</strong></td> <td><strong>Telefono</strong></td> <td><strong>Atendido por</strong></td>   <td><strong>Canal</strong></td>  <td><strong>Medidas</strong></td>  <td><strong>Fecha</strong></td>  <td><strong>Hora</strong></td>  <td><strong>Direccion</strong></td></tr>
			@foreach  ($cliente as $clientes)
      	
			<tr>
				<td>{!! $clientes->created_at !!}</td>
				<td>{!! $clientes->nombre !!}</td>
				<td>{!! $clientes->cedula !!}</td>  
				<td>{!! $clientes->telefono !!}</td>
				<td>{!! $clientes->atendido !!}</td>

			@foreach ($canal as $canales)
				@if($canales->cedula_cliente == $clientes->cedula)
				<td>{!! $canales->canal !!}</td>
				@endif
			@endforeach
		
			@foreach ($citamedida as $citamedidas)

			@foreach ($cita as $citas)

			@if ($citamedidas->cedula_cliente == $clientes->cedula)
			@if ($citamedidas->id_cita == $citas->id && $cont == 0)
			
			<td>si</td>
			<td>{!! $citas->fecha !!}</td>
			<td>{!! $citas->hora !!}</td>
			<td>{!! $clientes->direccion !!}</td>
			</tr>
				<?php $cont = 1; ?>
			@endif
			@endif
			@endforeach
			@endforeach
	       <?php $cont = 0; ?>
		@endforeach
	
		</table>
	</div>

          			</div>
          		</div>
          	</div>
	

		</section><! --/wrapper -->
      </section><!-- /MAIN CONTENT -->

      <!--main content end-->
      <!--footer start-->
      
      </footer>
      <!--footer end-->
  </section>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="../assets/js/jquery.js"></script>
    <script src="../assets/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="../assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="../assets/js/jquery.scrollTo.min.js"></script>
    <script src="../assets/js/jquery.nicescroll.js" type="text/javascript"></script>


    <!--common script for all pages-->
    <script src="../assets/js/common-scripts.js"></script>

    <!--script for this page-->
	<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>    
    <script src="../assets/js/tasks.js" type="text/javascript"></script>

    <script>
      jQuery(document).ready(function() {
          TaskList.initTaskWidget();
      });

      $(function() {
          $( "#sortable" ).sortable();
          $( "#sortable" ).disableSelection();
      });

    </script>
    
    
  <script>
      //custom select box

      $(function(){
          $('select.styled').customSelect();
      });

  </script>


