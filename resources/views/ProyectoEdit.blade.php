
<?php

	foreach ($proyecto as $key) {
		$nombre = $key->nombre;
	}

?>


<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title>D'Closet Home solutions - clientes</title>

    <!-- Bootstrap core CSS -->
    <link href="../../assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="../../assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    
    <!-- Custom styles for this template -->
    <link href="../../assets/css/style.css" rel="stylesheet">
    <link href="../../assets/css/style-responsive.css" rel="stylesheet">
    <link rel="../../stylesheet" href="assets/css/to-do.css">    



<section id="container" >
      <!-- **********************************************************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      *********************************************************************************************************************************************************** -->
      <!--header start-->
      <header class="header black-bg">
              <div class="sidebar-toggle-box">
                  <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
              </div>
            <!--logo start-->
            <a href="/" class="logo"><b>D'Closet</b></a>
            <!--logo end-->
            
            <div class="top-menu">
                <ul class="nav pull-right top-menu">
                    <li><a class="logout" href="auth/logout">Cerrar sesión</a></li>
                </ul>
            </div>

        </header>
      <!--header end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN SIDEBAR MENU
      *********************************************************************************************************************************************************** -->
      <!--sidebar start-->
     <aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu" id="nav-accordion">
              
                  <p class="centered"><a href="profile.html"><img src="../../assets/images/ui-sam.jpg" class="img-circle" width="60"></a></p>
                  <h5 class="centered">{!!$user=Auth::user()->name!!}</h5>
                    
                  <li class="mt">
                      <a class="active" href="/">
                          <i class="fa fa-dashboard"></i>
                          <span>Dashboard</span>
                      </a>
                  </li>

                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-desktop"></i>
                          <span>Clientes</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="/clientes">General</a></li>
                          <li><a  href="/clientes/trafico">Tráfico</a></li>
                          <li><a  href="/registro">Registrar nuevo</a></li> 
                      </ul>
                  </li>

                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-cogs"></i>
                          <span>Proyectos</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="/proyecto">General</a></li>
                          <li><a  href="/proyecto/nuevo">Nuevo</a></li>
                        <!--  <li><a  href="todo_list.html">Todo List</a></li> !-->
                      </ul>
                  </li>
                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-book"></i>
                          <span>Control de citas</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="/citasM">Nueva cita de medidas</a></li>
                          <li><a  href="/citasShow">Ver citas de medidas</a></li>
                          <li><a  href="blank.html">Nueva cita de presupuesto</a></li>
                          <li><a  href="/CitasPShow">Ver citas de presupuesto</a></li>
                          
                      </ul>
                  </li>
                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-tasks"></i>
                          <span>Gestión de pagos</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="/pagos/list/all">Administrar pagos</a></li>
                      </ul>
                  </li>

              </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>
      <!--sidebar end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">      	
          	<!-- SIMPLE TO DO LIST -->
          	<div class="row mt">
          		<div class="col-md-12">
          			<div>

          				<center><h1>{!!$nombre!!}</h1></center>




<b>Datos del cliente:</b>
<br>
@foreach($clienteData as $cliente)

<label>Nombre: </label> {!!$cliente->nombre!!}	
<br>
<label>Cedula: </label> {!!$cliente->cedula!!}
<br>
<label>Direccion: </label> {!!$cliente->direccion!!}
<br>
<label>Telefono: </label> {!!$cliente->telefono!!}
<br>
<FORM method="POST" action="/clientes/ver/{!!$cliente->cedula!!}"><button type="submit" class="btn btn-default"  name="ver">Ver</button></FORM>
@endforeach


<b><h1>Datos del proyecto</h1></b>
<br>
<table class="table table-striped table-hover">
@foreach($proyecto as $project)
<tr>
<td><label>Estado: </label></td>
	<td>{!!$project->estado!!}</td>
	<br>
</tr>
<tr>
<td><label>Producto: </label></td>
	<td>{!!$project->producto!!}</td>
</tr>
<tr>
<td><label>Linea: </label></td>
	<td>{!!$project->linea!!}</td>
</tr>
<tr>
	<td><label>Color: </label></td>
	<td>{!!$project->color!!}</td>
	</tr>
	<tr>

	@if($project->producto == 'cocina')
	<tr>
		<td><label>Estructura interna: </label></td>
		<td>{!!$project->estructura!!}</td>
	</tr>
	<tr>
		<td><label>Tope: </label></td>
		<td>{!!$project->tope!!}</td>
	</tr>
	<tr>
		<td><label>Rodatope: </label></td>
		<td>{!!$project->rodatope!!}</td>
	</tr>


	@endif

	@if($project->producto != 'vestier')
		<tr>
		<td><label>Tipo de Apertura: </label></td>
		<td>{!!$project->puerta!!}</td>
		</tr>
	@endif
	<tr>
	<td><label>Manillas: </label></td>
	<td>{!!$project->manillas!!}</td>
	</tr>
	<tr>
	<td><label>Costo</label></td>
	<td>{!!$project->total!!} BsF</td>
	</tr>
	<tr>
	<td><label>Observaciones: </label></td>
	<td>{!!$project->obs!!}</td>
	</tr>
</table>
	@if($CPdata == NULL)
		<FORM method="GET" action="/CP/{!!$project->id!!}">
				<button type="submit" class="btn btn-defalut">Emitir contenido de proyecto</button>
			</FORM>
		

	@else


	

		@foreach($CPdata as $cp)		
			<FORM method="GET" action="/CP/ver/{!!$project->id!!}">
				<button type="submit" class="btn btn-default">Ver contenido de proyecto</button>
			</FORM>					

		@endforeach
	@endif
  @if($project->estado != "Planteado")
      <FORM method="GET" action="/presupuesto/ver/{!!$project->id!!}">
        <button type="submit" class="btn btn-default">Ver información presupuesto</button>
      </FORM> 
  @endif
@endforeach

@if($project->estado != "Planteado" && $project->estado != "Instalado")  
<FORM method="GET" action="/proyecto/execute/{!!$project->id!!}">
  <button type="submit" class="btn btn-theme">Marcar como ejecutado</button>
</FORM> 
@endif

<FORM method="GET" action="../../proyecto/update/{!!$project->id!!}">
  <button type="submit" class="btn btn-theme"><i class="fa fa-cog"></i>Editar</button>
  </FORM>   
  </div>

  <! -- MODALS -->
@if($project->estado == "Planteado")              
            <button class="btn btn-danger" data-toggle="modal" data-target="#myModal">
              Anular Proyecto
            </button>

            
            <!-- Modal -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Anular proyecto</h4>
                  </div>
                  <div class="modal-body">
                    ¿Está seguro que desea eliminar el proyecto {!!$nombre!!}? mas tarde no podrá recuperarlo
                  </div>
                  <div class="modal-footer">
                    <FORM method="POST" action="/proyecto/edit/{!!$project->id!!}/delete">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    
                    <button type="submit" class="btn btn-primary">Confirmar</button>
                    </FORM>   
                  </div>
                </div>
              </div>
            </div>              
@endif



          			</div>
          		</div>
          	</div>
	

		</section><! --/wrapper -->
      </section><!-- /MAIN CONTENT -->

      <!--main content end-->
      <!--footer start-->
      
      </footer>
      <!--footer end-->
  </section>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="../../assets/js/jquery.js"></script>
    <script src="../../assets/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="../../assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="../../assets/js/jquery.scrollTo.min.js"></script>
    <script src="../../assets/js/jquery.nicescroll.js" type="text/javascript"></script>


    <!--common script for all pages-->
    <script src="../../assets/js/common-scripts.js"></script>

    <!--script for this page-->
	<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>    
    <script src="../../assets/js/tasks.js" type="text/javascript"></script>

    <script>
      jQuery(document).ready(function() {
          TaskList.initTaskWidget();
      });

      $(function() {
          $( "#sortable" ).sortable();
          $( "#sortable" ).disableSelection();
      });

    </script>
    
    
  <script>
      //custom select box

      $(function(){
          $('select.styled').customSelect();
      });

  </script>

