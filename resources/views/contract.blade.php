﻿<style type="text/css">
  td {
   border: 1px solid black;
   font-size: 12px;
   text-align: center;
}
  th {
    border: 1px solid black;
    text-align: center;
   
  }
  table{
    border-collapse: collapse;
  }
  .header
  {
    font-size: 10px;
  }
  #container{
    overflow: hidden;
  }
  #project{
    float: left;
  }
  #invoice{
    float: right;
  }
</style>
<html>
<head>
  <link rel="stylesheet" type="text/css" href="/public/assets/css/contratoStyle.css">
  <div class="header">
    <img src="assets/images/dclosetlogo.png"><br>
    D'Closet, c.a<br>
    RIF: J-40459192-3<br>
    Cumaná<br>
    Av. Gran Mariscal frente a plaza Martí<br>
  </div>
                    <center><p>Contrato</p></center>
</head>
<body>
  @foreach($price as $data)
  Fecha de elaboración: <?php setlocale (LC_TIME, "es_VE"); echo strftime("%A"); ?>
  @endforeach

    @foreach($cliente as $clienteData)
        <table>
        <tr><th colspan="2">Datos del cliente</th></tr>
        <tr>   
        <td><b>Nombre y apellidos</b></td><td>{!!$clienteData->nombre!!}</td>
        </tr>
        <tr>
        <td><b>C.I / RIF</b></td> <td>V-{!!$clienteData->cedula!!}</td>
        </tr>
        <tr><td><b>Dirección</b></td><td>{!!$clienteData->direccion!!}</td></tr>
        <tr><td><b>Estado</b></td> <td>Sucre</td></tr>
        <tr><td><b>Teléfono de contacto</b></td> <td>{!!$clienteData->telefono!!}</td></tr>
        <tr><td><b>E-mail</b></td> <td>{!!$clienteData->email!!}</td></tr>
        </table>
  @endforeach 

  <br>
  <center><b>Datos del producto adquirido</b></center>
<div id="container">
  <div id="project">
    @foreach($projects as $arrayP)

      @foreach($arrayP as $get)
          <table>
          <tr><td><b>Producto</b></td>
          
          <td>{!!$get->producto!!}</td>
          </tr>
          <tr><td><b>Linea</b></td><td>{!!$get->linea!!}</td></tr>
          @if($get->producto != "vestier")
            <tr><td><b>Tipo/Apertura </b></td><td>{!!$get->puerta!!}</td></tr>
          @endif  
          <tr><td><b>Manillas </b></td><td>{!!$get->manillas!!}</td></tr>
          
          <tr><td><b>Color</b></td><td>{!!$get->color!!}</td></tr>
          
          
          @if($get->producto != "vestier")
            <tr><td><b>Acabado de puertas </b></td><td>{!!$get->acabado!!}</td></tr>
          @endif
          @if($get->producto == "cocina")  
            <tr><td><b>Estructura interna</b></td><td>{!!$get->estructura!!}</td></tr>
            <tr><td><b>Tope</b></td><td>{!!$get->tope!!}</td></tr>
          @endif  
        </table>
      @endforeach

    @endforeach
  </div>
  <div id="invoice">
      <table>
        <tr><th>Segunda table</th></tr>
          <tr><td>Holis</td></tr>
      </table>
  </div>  
</div>

<center><b>Condiciones pago del contrato</b></center>

@foreach($price as $condition)
  <table>
  <tr><td><b>Reserva</b></td><td>{!!$condition->reserva!!} BsF</td></tr>
  
  @if($condition->cuotas > 1)
    <tr><td><b>Inicial</b></td><td>{!!$condition->inicial!!} BsF</td></tr>
    <tr><td><b>Segundo pago</b></td><td>{!!$condition->inicial!!} BsF</td></tr>
    <tr><td><b>Monto Final</b></td><td>{!!$condition->total!!} BsF</td></tr>
  @endif
  @if($condition->cuotas == 1)
    <tr><td><b>Número de cuotas</b></td><td>Pago único</td></tr>
    <tr><td><b>Monto</b></td><td>{!!$condition->total!!} BsF</td></tr>
  @endif    
  <tr><td><b>Total a cancelar antes de la entrega</b></td><td>{!!$condition->total!!} BsF</td></tr>  
  <tr><td><b>Monto financiado</b></td><td>{!!$condition->inicial!!} BsF</td></tr>  
  <tr><td><b>Total a cancelar al instalar</b></td><td>0 BsF</td></tr>
  </table>
@endforeach 
</body>
  

</html>