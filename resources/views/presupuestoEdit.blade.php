<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title>D'Closet Home solutions - clientes</title>

    <!-- Bootstrap core CSS -->
    <link href="/assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    
    <!-- Custom styles for this template -->
    <link href="/assets/css/style.css" rel="stylesheet">
    <link href="/assets/css/style-responsive.css" rel="stylesheet">
    <link rel="/stylesheet" href="/assets/css/to-do.css">    



<section id="container" >
      <!-- **********************************************************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      **************************************************************************************************************************************************s********* -->
      <!--header start-->
      <header class="header black-bg">
              <div class="sidebar-toggle-box">
                  <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
              </div>
            <!--logo stsart-->
            <a href="/" class="logo"><b>D'Closet</b></a>
            <!--logo end-->
            
            <div class="top-menu">
                <ul class="nav pull-right top-menu">
                    <li><a class="logout" href="auth/logout">Cerrar sesión</a></li>
                </ul>
            </div>

        </header>
      <!--header end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN SIDEBAR MENU
      *********************************************************************************************************************************************************** -->
      <!--sidebar start-->
     <aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu" id="nav-accordion">
              
                  <p class="centered"><a href="profile.html"><img src="/assets/images/ui-sam.jpg" class="img-circle" width="60"></a></p>
                  <h5 class="centered">{!!$user=Auth::user()->name!!}</h5>
                    
                  <li class="mt">
                      <a class="active" href="/">
                          <i class="fa fa-dashboard"></i>
                          <span>Dashboard</span>
                      </a>
                  </li>

                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-desktop"></i>
                          <span>Clientes</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="/clientes">General</a></li>
                          <li><a  href="/clientes/trafico">Tráfico</a></li>
                          <li><a  href="/registro">Registrar nuevo</a></li> 
                      </ul>
                  </li>

                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-cogs"></i>
                          <span>Proyectos</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="/proyecto">General</a></li>
                          <li><a  href="proyecto/nuevo">Nuevo</a></li>
                        <!--  <li><a  href="todo_list.html">Todo List</a></li> !-->
                      </ul>
                  </li>
                  
                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-book"></i>
                          <span>Control de citas</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="/citasM">Nueva cita de medidas</a></li>
                          <li><a  href="/citasShow">Ver citas de medidas</a></li>
                          <li><a  href="blank.html">Nueva cita de presupuesto</a></li>
                          <li><a  href="/CitasPShow">Ver citas de presupuesto</a></li>
                          
                      </ul>
                  </li>
                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-tasks"></i>
                          <span>Gestión de pagos</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="/pagos/list/all">Administrar pagos</a></li>
                      </ul>
                  </li>

              </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>
      <!--sidebar end-->
      <?php
  $total = 0;
  $subtotal = 0;
  $impuesto = 0;
?>
      
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">       
            <!-- SIMPLE TO DO LIST -->
            <div class="row mt">
              <div class="col-md-12">
                <div class="white-panel pn">

                    
        <center><H1>Presupuesto</H1></center>
        <br>
        <b>Datos del cliente</b>
        <br>
        @foreach($data as $datos)
        <label><b>Nombre: </b></label>
        {!!$datos->nombre!!}
        <br>
        <label><b>Cédula de identidad: </b></label>
        {!!$datos->cedula!!}
        <br>
        <label><b>Dirección: </b></label>
        {!!$datos->direccion!!}
        <br>
        <label><b>Telefono: </b></label>
        {!!$datos->telefono!!}
        <br></center>
        @endforeach
        </div>
      </div>
    </div>
        <br>
        @foreach($clienteProyecto as $Cliente)
        @foreach($proyectos as $proyecto)
        @if($proyecto->id == $Cliente->id_proyecto)

          
            <div class="row mt">
              <div class="col-lg-12">
                <div class="form-panel">
                <h4 class="mb"><i class="fa fa-angle-right"></i> Datos del proyecto</h4>
                <form class="form-inline" role="form">
                   <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Nombre</label>
                    <div class="col-sm-10">
                    <input type="text"  class="form-control" value="{!!$proyecto->nombre!!}" disabled>
                    </div>
                    </div>
                    <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Subtotal</label>
                    <div class="col-sm-10">
                    <input type="text"  class="form-control" value="{!!$proyecto->subtotal!!} BsF" required>
                    </div>
                    </div>
                    <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Impuesto:</label>
                    <div class="col-sm-10">
                    <input type="text"  class="form-control" value="{!!$proyecto->impuesto!!} BsF" required>
                    </div>
                    </div>
                    <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Total</label>
                    <div class="col-sm-10">
                    <input type="text"  class="form-control" value="{!!$proyecto->total!!} BsF" required>
                    </div>
                    </div>
            <?php
              $total = $total + $proyecto->total;
              $impuesto = $impuesto + $proyecto->impuesto;
              $subtotal = $subtotal + $proyecto->subtotal;
            ?>
            </form>
            </div>
                      
                </div><!-- /form-panel -->
              </div><!-- /col-lg-12 -->
            </div>
          
          <br>
        @endif
        @endforeach
        @endforeach
        <br><br>
        <div class="row mt">
        <div class="col-lg-12">
        <div class="form-panel">
        <h4 class="mb"><i class="fa fa-angle-right"></i> Información de presupesto</h4>
        <form method="POST" class="form-horizontal style-form" action="/presupuesto/{!!$datos->cedula!!}/contrato">

            <div class="form-group">
            <label class="col-sm-2 col-sm-2 control-label">Reserva</label>
            <div class="col-sm-10">
            <input type="text" id="reserva" class="form-control" name="reserva" required>
            </div>
            </div>
            <div class="form-group">
            <label class="col-sm-2 col-sm-2 control-label">Subtotal</label>
            <div class="col-sm-10">
            <input type="text" id="sub" class="form-control" name="sub" required>
            </div>
            </div>
            <div class="form-group">
            <label class="col-sm-2 col-sm-2 control-label">Descuento</label>
            <div class="col-sm-10">
            <input type="text" id="desc" class="form-control" name="desc" required>
            </div>
            </div>
            <div class="form-group">
            <label class="col-sm-2 col-sm-2 control-label">Impuesto</label>
            <div class="col-sm-10">
            <input type="text" id="impuesto" class="form-control" name="impuestoP" required>
            </div>
            </div>
            <div class="form-group">
            <label class="col-sm-2 col-sm-2 control-label">Total</label>
            <div class="col-sm-10">
            <input type="text" id="totalp" class="form-control" name="totalP" required>
            </div>
            </div>
            <div class="form-group">
            <label class="col-sm-2 col-sm-2 control-label">Ahorro</label>
            <div class="col-sm-10">
            <input type="text" id="ahorro" class="form-control" name="ahorro" required>
            </div>
            </div>
            <br>
            <div class="form-group">
            <label class="col-sm-2 col-sm-2 control-label">Tiempo de ejecución</label>
            <div class="col-sm-10">
            <input type="text" id="tiempo" class="form-control" name="tiempo" required>
            </div>
            </div>
            <div class="form-group">
            <label class="col-sm-2 col-sm-2 control-label"></label>
            <div class="col-sm-10">
            <select class="form-control" name="UT" id="UT" required>
              <option value="días">Días</option>
              <option value="Semanas">Semanas</option>
              <option value="Meses">Meses</option>
              </select>
            </div>
            </div>
        <br>
            <div class="form-group">
            <label class="col-sm-2 col-sm-2 control-label">Forma de pago</label>
            <div class="col-sm-10">
            <select class="form-control" name="forma" id="forma" required>
            <option value="Cheque">Cheque</option>
            <option value="Efectivo">Efectivo</option>
            <option value="Transferencia">Transferencia</option>
            </select>
            </div>
            </div>

        <div class="form-group">
        <label class="col-sm-2 col-sm-2 control-label">Banco</label>
        <div class="col-sm-10">
        <input type="text" class="form-control" name="banco">
        </div>
      </div>
        <div class="form-group">
        <label class="col-sm-2 col-sm-2 control-label">Número cheque/transferencia</label>
        <div class="col-sm-10">
        <input type="text" class="form-control" name="ref">
        </div>
        </div>
        <label><b>Financiamiento</b></label>
        
        <br>
        <div class="form-group">
        <label class="col-sm-2 col-sm-2 control-label">Número de cuotas</label>
        <div class="col-sm-10">
        <input type="number" class="form-control" name="cuotas">
        </div>
        </div>
        
          <button type="submit" class="btn btn-theme">Crear Contrato</button>
        </form>
        
        <form method="POST" action="/clientes/ver/{!!$datos->cedula!!}">
        <button type="submit" class="btn">Regresar</button>
        </form>

                </div>
              </div>
            </div>
   </div>
              </div><!-- col-lg-12-->       
            </div>

    </section><! --/wrapper -->
      </section><!-- /MAIN CONTENT -->

      <!--main content end-->
      <!--footer start-->
      
      </footer>
      <!--footer end-->
  </section>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="../assets/js/jquery.js"></script>
    <script src="../assets/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="../assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="../assets/js/jquery.scrollTo.min.js"></script>
    <script src="../assets/js/jquery.nicescroll.js" type="text/javascript"></script>


    <!--common script for all pages-->
    <script src="../assets/js/common-scripts.js"></script>

    <!--script for this page-->
  <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>    
    <script src="../assets/js/tasks.js" type="text/javascript"></script>

    <script>
      jQuery(document).ready(function() {
          TaskList.initTaskWidget();
      });

      $(function() {
          $( "#sortable" ).sortable();
          $( "#sortable" ).disableSelection();
      });

    </script>
    
    
  <script>
      //custom select box

      $(function(){
          $('select.styled').customSelect();
      });

  </script>



<script type="text/javascript">
    
    Unique = document.getElementById("uniq");

    function cuotas(fallout)
    {
      
        if(fallout == "1")
        {
          Unique.style.visibility = 'visible';
        }
        if(fallout == "2")
        {
          PagoUnico.style.visibility = 'hidden';
        }
    }

    function descuento(porc)
    {
      
      porc = document.getElementById('desc').value;
      porc = parseFloat(porc);
        if(porc > 0)
        {
          porc = parseFloat(porc)/100;
          

          subtotal = document.getElementById("sub").value;
          impuesto = document.getElementById('impuestop').value;
          total = document.getElementById("totalp").value;
          
          impuestoN = parseFloat(impuesto) * porc;
          

          subtotal = parseFloat(subtotal)*porc;
          impuestoNuevo = parseFloat(impuesto) - impuestoN;
          totalN = parseFloat(total)*porc;
          totalNuevo = parseFloat(total) - totalN;
          
          
          document.getElementById("impuestop").value = impuestoNuevo;
          document.getElementById("totalp").value = totalNuevo;
          document.getElementById("ahorro").value = Math.abs(totalNuevo - parseFloat(total));
        }
    }

</script>

