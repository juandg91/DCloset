<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title>D'Closet Home solutions - clientes</title>

    <!-- Bootstrap core CSS -->
    <link href="../../assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="../../assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    
    <!-- Custom styles for this template -->
    <link href="../../assets/css/style.css" rel="stylesheet">
    <link href="../../assets/css/style-responsive.css" rel="stylesheet">
    <link rel="../../stylesheet" href="assets/css/to-do.css">    



<section id="container" >
      <!-- **********************************************************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      *********************************************************************************************************************************************************** -->
      <!--header start-->
      <header class="header black-bg">
              <div class="sidebar-toggle-box">
                  <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
              </div>
            <!--logo start-->
            <a href="/" class="logo"><b>D'Closet</b></a>
            <!--logo end-->
            
            <div class="top-menu">
                <ul class="nav pull-right top-menu">
                    <li><a class="logout" href="auth/logout">Cerrar sesión</a></li>
                </ul>
            </div>

        </header>
      <!--header end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN SIDEBAR MENU
      *********************************************************************************************************************************************************** -->
      <!--sidebar start-->
     <aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu" id="nav-accordion">
              
                  <p class="centered"><a href="profile.html"><img src="../../assets/images/ui-sam.jpg" class="img-circle" width="60"></a></p>
                  <h5 class="centered">{!!$user=Auth::user()->name!!}</h5>
                    
                  <li class="mt">
                      <a class="active" href="index.html">
                          <i class="fa fa-dashboard"></i>
                          <span>Dashboard</span>
                      </a>
                  </li>

                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-desktop"></i>
                          <span>Clientes</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="../../clientes">General</a></li>
                          <li><a  href="../../clientes/trafico">Tráfico</a></li>
                          <li><a  href="../../registro">Registrar nuevo</a></li> 
                      </ul>
                  </li>

                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-cogs"></i>
                          <span>Proyectos</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="../../proyecto">General</a></li>
                          <li><a  href="../../proyecto/nuevo">Nuevo</a></li>
                        <!--  <li><a  href="todo_list.html">Todo List</a></li> !-->
                      </ul>
                  </li>
                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-book"></i>
                          <span>Control de citas</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="../../citasM">Nueva cita de medidas</a></li>
                          <li><a  href="../../citasShow">Ver citas de medidas</a></li>
                          <li><a  href="blank.html">Nueva cita de presupuesto</a></li>
                          <li><a  href="../../CitasPShow">Ver citas de presupuesto</a></li>
                          
                      </ul>
                  </li>
                  

              </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>
      <!--sidebar end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">      	
          	<!-- SIMPLE TO DO LIST -->
          	<div class="row mt">
          		<div class="col-md-12">
          			<div>




<b><h1>Pago pendiente</h1></b>

  @foreach($pagos as $pago)
 
  <form  class="form-horizontal style-form" method="POST">
            <div class="form-group">
            <label class="col-sm-2 col-sm-2 control-label">Monto</label>
            <div class="col-sm-10"> 
            <input type="text" name="cantidad" value="{!!$pago->abono!!} BsF" class="form-control round-form">
            </div>
            </div>

            <div class="form-group">
            <label class="col-sm-2 col-sm-2 control-label">Fecha de vencimiento</label>
            <div class="col-sm-10"> 
            <input type="text" value="{!!$pago->fecha!!}" class="form-control round-form" disabled>
            </div>
            </div>

            <div class="form-group">
            <label class="col-sm-2 col-sm-2 control-label">Fecha de pago</label>
            <div class="col-sm-10"> 
            <input type="date" name="fecha" class="form-control round-form" required>
            </div>
            </div>

            <div class="form-group">
            <label class="col-sm-2 col-sm-2 control-label">Concepto</label>
            <div class="col-sm-10"> 
            <input type="text" name="concepto" value="{!!$pago->concepto!!}" class="form-control round-form" required>
            </div>
            </div>

            <div class="form-group">
            <label class="col-sm-2 col-sm-2 control-label">Método de pago</label>
            <div class="col-sm-10"> 
            <select class="form-control" name="metodo" id="metodo" required>
            <option value="Cheque">Cheque</option>
            <option value="Efectivo">Efectivo</option>
            <option value="Transferencia">Transferencia</option>
          </select>
            </div>
          </div>
            <div class="form-group">
            <label class="col-sm-2 col-sm-2 control-label">Banco</label>
            <div class="col-sm-10"> 
            <input type="text" name="banco" class="form-control round-form">
            </div>
            </div>
            <div class="form-group">
            <label class="col-sm-2 col-sm-2 control-label">Número de cheque/transferencia</label>
            <div class="col-sm-10"> 
            <input type="text" name="ref" class="form-control round-form">
            </div>
            </div>
            

          			</div>
          		</div>
          	</div>
          
            <! -- MODALS -->
              <div class="showback">
                
            <!-- Button trigger modal -->
            <button type="button" class="btn btn-theme" data-toggle="modal" data-target="#myModal">
              Registrar pago
            </button>
            
            <!-- Modal -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Registrar pago</h4>
                  </div>
                  <div class="modal-body">
                    ¿Desea imprimir recibo?
                  </div>
                  <div class="modal-footer">
                    <button type="submit" class="btn btn-default" name="no" value="no">No</button>
                  
                    <button type="submit" class="btn btn-primary" name="si" value="si">Si</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                  </div>
                </div>
              </div>
            </div>              
              </div><!-- /showback -->
    </form>
	@endforeach

		</section><! --/wrapper -->
      </section><!-- /MAIN CONTENT -->

      <!--main content end-->
      <!--footer start-->
      
      </footer>
      <!--footer end-->
  </section>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="../../assets/js/jquery.js"></script>
    <script src="../../assets/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="../../assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="../../assets/js/jquery.scrollTo.min.js"></script>
    <script src="../../assets/js/jquery.nicescroll.js" type="text/javascript"></script>


    <!--common script for all pages-->
    <script src="../../assets/js/common-scripts.js"></script>

    <!--script for this page-->
	<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>    
    <script src="../../assets/js/tasks.js" type="text/javascript"></script>

    <script>
      jQuery(document).ready(function() {
          TaskList.initTaskWidget();
      });

      $(function() {
          $( "#sortable" ).sortable();
          $( "#sortable" ).disableSelection();
      });

    </script>
    
    
  <script>
      //custom select box

      $(function(){
          $('select.styled').customSelect();
      });

  </script>