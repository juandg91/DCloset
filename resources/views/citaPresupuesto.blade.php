<!DOCTYPE html>
<html>
  <head>
    <title>Registro de citas</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" 
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="../../assets/js/html5shiv.js"></script>
      <script src="../../assets/js/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <ul class="nav nav-tabs nav-justified">
    <li class="active"><a href="#">Inicio</a></li>
    <li><a href="#">Inicio</a></li>
    <li><a href="#">Registro</a></li>
    <li><a hef="#">Proyectos</a></li>
    <li><a href="/citasM">Citas</a></li>
    </ul>
    <div id="wrap">
      <div class="container-fluid">
        <div>
        <img src="images/dclosetlogo.png" class="img-rounded img-responsive">
      </div>
        <center><H1>Registro de citas</H1></center>

        <div class="center-block">
          <form class="form-horizontal" role="form" method="POST">
            <div class= "form-group row-xs-4">
            <label>Fecha</label> <input type="date" class="form-control">
          </div>
            <div class = "form-group row-xs-4" >
            <label>Observaciones</label> <input type="textarea" class="form-control">
          </div>
           
          <button type="submit" class="btn btn-primary active">Aceptar</button>
          </form>
        </div>
      </div>
    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="//code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="{{ assets(assets/js/bootstrap.min.js) }}"></script>
  </body>
</html>