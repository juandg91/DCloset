
<html>
<head>
	<link href="/assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
        
    <!-- Custom styles for this template -->
    <link href="/assets/css/style.css" rel="stylesheet">
    <link href="/assets/css/style-responsive.css" rel="stylesheet">

    <link href="/assets/css/table-responsive.css" rel="stylesheet">
	<title>Recibo de pago</title>
	<img src="assets/images/dclosetlogo.png">
	<p>D'Closet, C.A<br>
        RIF: J-40459192-3<br>Cumaná, estado Sucre<br>
        Av. Gran Mariscal frente a plaza Martí</p>
</head>
<body>
	<center><h2>RECIBO DE PAGO</h2></center>
	<br>
	<table class="table table-striped table-hover">
		<tr><td><b>DATOS DEL CLIENTE</b></td></tr>
		@foreach($cliente as $datos)
			<tr><td><b>Nombre y Apellidos</b></td>
			<td>{!!$datos->nombre!!}</td></tr>
			<tr>
				<td><b>C.I/RIF</b></td><td>{!!$datos->cedula!!}</td><td><b>Num. Contrato</b></td><td></td>
				<tr><td><b>Dirección fiscal</b></td><td>{!!$datos->direccion!!}</td></tr>
			</tr>
		@endforeach
	</table>

	<br>
	<table class="table table-striped table-hover">
		<tr><td><b>DATOS DEL PAGO</b></td></tr>
		@foreach($pago as $pagos)
		<tr><td><b>Monto BsF</b></td><td>{!!$pagos->abono!!}</td><td><b>Saldo restante</b></td><td> {!!$deuda!!} BsF</td></tr>
		<tr><td><b>Método de pago</b></td><td>{!!$pagos->forma!!}</td><td><b>Banco</b></td><td>{!!$pagos->banco!!}</td><td><b>Ref</b></td><td>{!!$pagos->ref!!}</td></tr>
		<tr><td><b>Fecha de vencimiento</b></td><td>{!!$pagos->fecha!!}</td><td><b>Fecha de emisión</b></td><td>{!!$pagos->fecha_abono!!}</td></tr>
		<tr><td><b>Ccncepto</b></td><td>{!!$pagos->concepto!!}</td></tr>
		<tr><td><b>Observaciones</b></td><td></td></tr>
		@endforeach
	</table>
	<br>
	<br><br><br><center>_________________________</center><br>
			<center>D'Closet</center>

	<script src="assets/js/jquery.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>
    <script src="assets/js/jquery.nicescroll.js" type="text/javascript"></script>


    <!--common script for all pages-->
    <script src="assets/js/common-scripts.js"></script>		
</body>


</html>