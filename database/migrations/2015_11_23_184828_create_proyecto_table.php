<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProyectoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proyecto', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('nombre', 200);
            $table->string('producto');
            $table->string('linea');
            $table->string('estructura');
            $table->string('puerta')->nullabel();
            $table->string('manillas')->nullable();
            $table->string('tope')->nullable();
            $table->string('rodatope')->nullable();
            $table->float('subtotal')->nullable();
            $table->float('impuesto')->nullable();
            $table->float('total')->nullable();
            $table->string('obs')->nullable();
        
            $table->string('estado')->default('Planteado');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('proyecto');
    }
}
