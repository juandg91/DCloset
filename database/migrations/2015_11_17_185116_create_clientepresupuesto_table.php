<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientepresupuestoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cliente_citapresupuesto', function(Blueprint $table)
        {
            $table->string('cedula_cliente');
            $table->foreign('cedula_cliente')->references('cedula')->on('cliente');
            $table->integer('id_cita')->unsigned();
            $table->foreign('id_cita')->references('id')->on('citapresupuesto');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cliente_citapresupuesto');
    }
}
