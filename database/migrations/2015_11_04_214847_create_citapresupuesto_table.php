<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCitapresupuestoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('citapresupuesto', function(Blueprint $table)
        {
            $table->increments('id');
            $table->date('fecha');
            $table->time('hora');
            $table->string('estado',22)->default('Pendiente');
            $table->string('obs',140)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('citapresupuesto');
    }
}
