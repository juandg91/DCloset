<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCitaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cita', function(Blueprint $table)
        {
            $table->increments('id');
            $table->date('fecha');
            $table->time('hora');
            $table->string('estado',10)->default('en espera');
            $table->string('obs',140)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cita');
    }
}
