<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePresupuestoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('presupuesto', function(Blueprint $table)
        {
            $table->increments('id');
            $table->float('subtotal');
            $table->float('impuesto');
            $table->float('total');
            $table->integer('tiempo');
            $table->integer('descuento');
            $table->float('inicial');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('presupuesto');
    }
}
