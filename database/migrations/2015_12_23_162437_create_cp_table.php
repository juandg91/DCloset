<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCpTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('CP', function(Blueprint $table)
        {
            $table->integer("item1");
            $table->integer("item2");
            $table->integer("item3");
            $table->integer("item4");
            $table->integer("item5");
            $table->integer("item6");
            $table->integer("item7");
            $table->integer("item8");
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('CP');
    }
}
