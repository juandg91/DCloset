<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientecanalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cliente_canal', function(Blueprint $table)
        {
            $table->string('cedula_cliente');
            $table->foreign('cedula_cliente')->references('cedula')->on('cliente');
            $table->string('canal');
            $table->string('otroobs',180)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cliente_canal');
    }
}
