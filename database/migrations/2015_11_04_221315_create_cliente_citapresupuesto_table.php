<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClienteCitapresupuestoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cliente_citapresupuesto', function(Blueprint $table)
        {
            $table->foreign('id-cliente')->references->('id')->on('cliente');
            $table->foreign('id-presupuesto')->references->('id')->on('presupuesto');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cliente_citapresupuesto');
    }
}
