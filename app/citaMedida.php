<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class citaMedida extends Model
{
    protected $table = 'cita';

    protected $fillable = ['fecha', 'estado', 'obs', 'hora'];

}

