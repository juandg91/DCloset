<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProyectoPresupuesto extends Model
{
    protected $table = 'proyecto_presupuesto';

    protected $fillable = ['id_presupuesto', 'id_proyecto'];
}
