<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class clienteCanal extends Model
{
    protected $table= 'cliente_canal';
    public $timestamps = false;

    protected $fillable = ['cedula_cliente', 'canal', 'otroobs'];
}
