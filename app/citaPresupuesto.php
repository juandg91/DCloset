<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class citaPresupuesto extends Model
{
    protected $table= 'citapresupuesto';

    protected $fillable = ['fecha', 'estado', 'obs', 'hora'];
}
