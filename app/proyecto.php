<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class proyecto extends Model
{
    //
    protected $table = 'proyecto';

    protected $fillable = ['nombre', 'producto', 'estado', 'linea', 'estructura', 'puerta', 'manillas', 'tope', 'rodatope', 'subtotal', 'impuesto', 'total', 'obs', 'color', 'modelo', 'acabado'];

}
