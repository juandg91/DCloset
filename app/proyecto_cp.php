<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class proyecto_cp extends Model
{
    protected $table = 'proyecto_cp';
    public $timestamps = false;
    protected $fillable = ['id_proyecto', 'id_cp'];
}
