<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class project_content extends Model
{
    protected $table = 'project_content';
    public $timestamps = false;

    protected $fillable = ['mueble base 30', 
        'mueble base 45',
        'mueble base 60',
        'mueble base 90',
        'despensa 30',
        'despensa 45',
        'despensa 60',
        'mueble base fregaplato 60',
        'mueble base fregaplato 90',
        'mueble base gavetero 60',
        'mueble base angulo',
        'torre horno 60',
        'torre horno 90',
        'empotramiento de nevera 80',
        'empotramiento de nevera 914',
        'wall 45',
        'wall 60',
        'wall 90',
        'wall platera 60',
        'wall platera 90',
        'separadores',
        'retrobases',
        'desayunador',
        'pantalla',
        'superficie',
        'adicional'];
}
