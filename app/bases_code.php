<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class bases_code extends Model
{
    protected $table= 'bases_code';

    protected $fillable = ['nombre_base', 'codigo', 'cantidad'];
}
