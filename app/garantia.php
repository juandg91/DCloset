<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class garantia extends Model
{
    protected $table = 'garantia';
    public $timestamps = false;

    protected $fillable = ['fecha', 'estado', 'observaciones'];
}
