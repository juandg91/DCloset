<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class producto extends Model
{
    protected $table = 'tipo';
    public $timestamps = false;

    protected $fillable = ['nombre'];
}
