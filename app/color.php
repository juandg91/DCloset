<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class color extends Model
{
    protected $table = 'linea_color';
    public $timestamps = false;

    protected $fillable = ['nombre_linea', 'producto', 'color'];
    
    public static function getColors($linea, $product)
    {
        return color::where('nombre_linea','=',$linea)->where('producto','=',$product)->get();
    }
}
