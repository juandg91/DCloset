<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class proyecto_cliente_garantia extends Model
{
    protected $table = 'proyecto_cliente_garantia';

    public $timestamps = false;

    protected $fillable = ['id_proyecto', 'cedula_cliente', 'id']; 
}
