<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class proyecto_ejecucion extends Model
{
    protected $table = 'proyecto_ejecucion';
    public $timestamps = false;

    protected $fillable = ['id_proyecto', 'id'];
}
