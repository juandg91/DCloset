<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class presupuesto extends Model
{
    protected $table = 'presupuesto';

    protected $fillable = ['id', 'subtotal', 'impuesto', 'total', 'tiempo', 'descuento', 'inicial','cuotas','ut','forma','codigo'];
}

