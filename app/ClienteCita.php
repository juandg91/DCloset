<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClienteCita extends Model
{
    protected $table = 'cliente_cita';
    public $timestamps = false;

    protected $fillable = ['cedula_cliente', 'id_cita'];
    
}
