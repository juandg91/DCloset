<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssistantModel extends Model
{
    protected $table = 'assist';

    protected $fillable = ['cedula', 'password', 'nombre'];
}
