<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class clienteCitaPresupuesto extends Model
{
    protected $table = 'cliente_citapresupuesto';
    public $timestamps = false;

    protected $fillable = ['cedula_cliente', 'id_cita'];

 //   public static function where($column, $operator = null, $value = null, $boolean = 'and'){
 //       return \Illuminate\Database\Eloquent\Builder::where($column, $operator, $value, $boolean);
 //   }
}

