<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class garantiaAnterior extends Model
{
    protected $table = 'garantiaAnterior';
    public $timestamps = false;

    protected $fillable = ['cliente', 'producto', 'fecha', 'tiempo', 'numero_contrato'];
}
