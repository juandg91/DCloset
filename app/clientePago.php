<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class clientePago extends Model
{
    
    protected $table= 'cliente_pago';
    public $timestamps = false;

    protected $fillable = ['cedula_cliente', 'id_pago'];

}
