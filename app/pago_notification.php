<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class pago_notification extends Model
{
    protected $table= 'pago_notification';
    public $timestamps = false;

    protected $fillable = ['id_pago', 'id_notification'];
}
