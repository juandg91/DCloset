<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class clienteProyecto extends Model
{
    protected $table = 'cliente_proyecto';
    public $timestamps = false;

    protected $fillable = ['cedula_cliente', 'id_proyecto'];
}
