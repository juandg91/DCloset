<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return Redirect::to('home');
});

Route::get('/pruebalogin', 'Auth\AuthController@getLogin');
Route::post('/pruebalogin', 'Auth\AuthController@postLogin');

Route::get('home', ['middleware' => 'auth', 'uses' => 'HomeController@index']);

Route::get('/clientes', ['middleware' => 'auth', 'uses' => 'clienteController@index']);
Route::get('/clientes/ver/{cedula}', 'clienteController@datos');
Route::post('/clientes/find', 'clienteController@find');

Route::get('/registro', ['middleware' => 'auth', 'uses' => 'clienteController@create']);
Route::post('/registro', 'clienteController@store');
Route::get('/registroSucces', ['middleware' => 'auth', 'uses' => 'clienteController@succes']);
//Route::post('/registro', 'HomeController@index');

//Dates routes
Route::get('/citas', ['middleware' => 'auth', 'uses' => 'citasController@index']);
Route::get('/citasM', ['middleware' => 'auth', 'uses' => 'citasController@createCitaMedida']);
Route::get('/citasM/{cedula?}', ['middleware' => 'auth', 'uses' => 'citasController@createCitaMedidaC']);

Route::get('/citasShow', ['middleware' => 'auth', 'uses' => 'citasController@show']);
Route::get('/citasShowPend', ['middleware' => 'auth', 'uses' => 'citasController@showPending']);
Route::get('/citasShowDone', ['middleware' => 'auth', 'uses' => 'citasController@showExecuted']);
Route::get('/citasShowOrdered', ['middleware' => 'auth', 'uses' => 'citasController@showDateOrder']);
Route::post('/citasM/', 'citasController@storeCitaMedida');
Route::post('/citasM/{cedula?}', 'citasController@storeCitaMedida');
Route::get('/citasP/{cedula}', ['middleware' => 'auth', 'uses' => 'citasController@createCitaPresupuesto']);
Route::post('/citasP/{cedula}', 'citasController@storeCitaPresupuesto');
Route::get('/CitasPShow', ['middleware' => 'auth', 'uses' => 'citasController@showPresupuestos']);
Route::get('/citasPShowOrdered', ['middleware' => 'auth', 'uses' => 'citasController@showPresupuestoOrdered']);
Route::get('/citasPShowPend', ['middleware' => 'auth', 'uses' => 'citasController@showPresupuestoPending']);
Route::get('/citasPShowDone', ['middleware' => 'auth', 'uses' => 'citasController@showPresupuestoExecuted']);
Route::post('/CitasSearchP', 'citasController@searchPresupuesto');
Route::post('/CitasSearch', 'citasController@searchCita');
Route::post('/citaEdit/{cedula}/{id}', 'citasController@edit');
Route::post('/citaEdit/{cedula}/{id}/confirm', 'citasController@editConfirm');
Route::get('/citapEdit/{cedula}/{id}', 'citasController@editP');
Route::post('/citapEdit/{cedula}/{id}/confirm', 'citasController@editpConfirm');


//End of Dates routes
Route::get('/assistants', 'AdminController@createAssistant');
Route::post('/posts/create', 'AdminController@store');

//Proyecto Routes
Route::get('/proyecto', ['middleware' => 'auth', 'uses' => 'proyectoController@index']);
Route::get('/proyecto/nuevo', ['middleware' => 'auth', 'uses' => 'proyectoController@create']);
Route::post('/proyecto/nuevo/{cedula}', 'proyectoController@store');
Route::get('/proyecto/nuevo/{cedula}', ['middleware' => 'auth', 'uses' => 'proyectoController@crear']);
#Route::get('/proyecto/nuevo/{linea}/{producto}', 'proyectoController@getColors');
Route::get('/proyecto/edit/{id}', ['middleware' => 'auth', 'uses' => 'proyectoController@edit']);
Route::post('/proyecto/edit/{id}/delete', 'proyectoController@delete');
Route::get('proyecto/update/{id}', ['middleware' => 'auth', 'uses' => 'proyectoController@update']);
Route::post('proyecto/update/{id}', 'proyectoController@saveUpdate');
Route::get('proyecto/execute/{id}', ['middleware' => 'auth', 'uses' => 'proyectoController@execute']);
Route::post('proyecto/execute/{id}', 'proyectoController@executeSave');

//Presupuesto Routes
Route::get('/presupuesto/{cedula}', ['middleware' => 'auth', 'uses' => 'PresupuestoController@generate']);
Route::post('/presupuesto/{cedula}/contrato', 'PresupuestoController@contrato');
Route::get('/presupuesto/ver/{id}', ['middleware' => 'auth', 'uses' => 'PresupuestoController@show']);

// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('/admin', ['middleware' => 'admin', function()
{
	return 'welcome to the admin page';
}]);

//clients routes
Route::get('clientes/trafico', ['middleware' => 'auth', 'uses' => 'clienteController@show']);

//Contenido de proyecto

Route::get('CP/{id}', ['middleware' => 'auth', 'uses' => 'CPController@nuevo']);
Route::post('CP/{id}', 'CPController@generator');
Route::get('CP/ver/{id}', ['middleware' => 'auth', 'uses' => 'CPController@show']);

Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');

Route::get('pdf/{id}', 'pdfController@invoice');
Route::get('recibos/{id}', 'pdfController@reciboPago');

//pagos
Route::get('pagos/{id}', ['middleware' => 'auth', 'uses' => 'pagoController@pay']);
Route::post('pagos/{id}', 'pagoController@Store');
Route::get('pagos/list/all', ['middleware' => 'auth', 'uses' => 'pagoController@show']);
Route::get('pagos/list/all/canceled', ['middleware' => 'auth', 'uses' => 'pagoController@viewAll']);
Route::post('pagos/list/all/canceled/datefilter', 'pagoController@filterByDate');
Route::post('pagos/list/all/canceled/clientfilter', 'pagoController@filterByClient');

//garantias
Route::get('/garantias/anteriores', ['middleware' => 'auth', 'uses' => 'warrantyController@Old']);
Route::post('/garantias/anteriores', 'warrantyController@store');
Route::get('/garantias/atencion', ['middleware' => 'auth', 'uses' => 'warrantyController@attention']);

Route::get('error', function(){ 
    abort(500);
});