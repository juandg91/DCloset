<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;

use Datetime;
use Carbon\Carbon;

use App\Http\Requests;
use App\Http\Requests\PostsRequest;
use App\Http\Controllers\Controller;
use App\cliente;
use App\citaMedida;
use App\ClienteCita;
use App\clienteCanal;
use App\clienteCitaPresupuesto;
use App\citaPresupuesto;
use App\proyecto;
use App\cliente_proyecto;
use App\notification;
use App\pagos;
use App\clientePago;

class ClienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users=DB::table('cliente')
        ->orderBy('created_at','desc')
        ->take(8)
        ->get();

        return view('Clientes', ['clientes' => $users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('inicio');
    }

    public function succes()
    {
        return view('');
    }

    public function datos($cedula)
    {
        $data = DB::table('cliente')->where('cedula', '=', $cedula)->get();
        $user_cita = DB::table('cliente_cita')->where('cedula_cliente', '=', $cedula)->get();
        $cita = citaMedida::all();
        $user_presupuesto = DB::table('cliente_citapresupuesto')->where('cedula_cliente', '=', $cedula)->get();
        $Citapresupuesto = citaPresupuesto::all();
        $user_proyecto = DB::table('cliente_proyecto')->where('cedula_cliente', '=', $cedula)->get();
        $proyecto = proyecto::all();
        $pagos = pagos::all();
        $clientePago = clientePago::where('cedula_cliente', $cedula)->get();


        return view('ClienteDatos', ['data' => $data, 'ClienteCita' => $user_cita, 'cita' => $cita, 'ClientePresupuesto' => $user_presupuesto, 'Citapresupuesto' => $Citapresupuesto, 'ClienteProyecto' => $user_proyecto, 'Proyecto' => $proyecto, 'pago' => $pagos, 'clientePago' => $clientePago]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if ($request->ajax())
        {
            cliente::create($request->all());

            return response()->json(['message' => 'Post saved']);
        }

       
/*        $user_data = DB::table('cliente')->where('cedula', '=', $request->input('cedula'))->get();

        if($user_data != NULL)
        {
            return ("Este usuario ya se encuentra registrado");
        }

        $cliente = new cliente;
        $cliente->cedula = $request->input('cedula');
        $cliente->nombre = $request->input('nombre');
        $cliente->direccion = $request->input('direccion');
        $cliente->telefono = $request->input('telefono');
        $cliente->atendido = $request->input('asistente');
        $cliente->email = $request->input('email');

        $cliente->save();

        $notification = new notification;

        $notification->descripcion = "Se ha registrado un nuevo cliente ".$cliente->nombre;
        $notification->save();
        
        $canal = new clienteCanal;
        $canal->cedula_cliente = $request->input('cedula');
        if($request->input('canal') == "Otro")
        {
            $canal->canal = "Otro: ".$request->input('canal-otro');
        }
        else
        {
            $canal->canal = $request->input('canal');
        }
        
        $canal->save();
        
        return view('clienteSucces',['data' => $cliente->cedula]); */
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $thisMonth = Carbon::now('m');

        $cliente = cliente::where('created_at', $thisMonth);
        $cita = citaMedida::all();
        $clienteCita = ClienteCita::all();
        $canal = clienteCanal::all();

        return view('Clientes/trafico', ['cliente' => $cliente, 'cita'=> $cita, 'citamedida' => $clienteCita, 'canal' => $canal, 'month' => $thisMonth]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    public function find(Request $request)
    {
        $token = $request->input('token');
        

        $users=DB::table('cliente')
        ->where('cedula','=', $token)
        ->orWhere('nombre','=', $token)
        ->get();

        return view('Clientes', ['clientes' => $users]);   
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
