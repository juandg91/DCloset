<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use Datetime;
use Carbon\Carbon;

use App\Http\Requests;
use App\Http\Requests\PostsRequest;
use App\Http\Controllers\Controller;
use App\presupuesto;
use App\cliente;
use App\proyecto;
use App\ProyectoPresupuesto;
use App\clienteProyecto;
use App\pagos;
use App\clientePago;


class PresupuestoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function notification($type)
    {

    }

    public function contract()
    {
      /*    $data = $this->getData();
        $date = date('Y-m-d');
        $contract = "222";
        $view = \View::make('contract', compact('data', 'date', 'contract'));
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view);
        $pdf->stream(); */
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML('<h1>Test</h1>');
        return $pdf->download();
    }

    public function getData()
    {
        $data = [
            'quantity' => '1',
            'description' => 'some random text',
            'price' => '500',
            'total' => '500'
        ];

        return $data;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function generate($cedula)
    {
        $data = DB::table('cliente')->where('cedula', '=', $cedula)->get();
        $clienteProyecto = DB::table('cliente_proyecto')->where('cedula_cliente', '=', $cedula)->get();
        $proyectos = DB::table('proyecto')->where('estado', '=', 'Planteado')->get();

        Return view('presupuesto', ['data' => $data, 'clienteProyecto' => $clienteProyecto, 'proyectos' => $proyectos]);
    }

    public function contrato(PostsRequest $request, $cedula)
    {
        $tiempo = $request->input('tiempo');

        $presupuesto = new presupuesto;
        

        $data = DB::table('cliente')->where('cedula', '=', $cedula)->get();
    
        $presupuesto->tiempo = $tiempo;
        $presupuesto->subtotal = $request->input('sub'); 
        $presupuesto->impuesto = $request->input('impuestoP');
        $presupuesto->total = $request->input('totalP');
        $presupuesto->descuento = $request->input('desc');
        $presupuesto->ahorro = $request->input('ahorro');
        $presupuesto->inicial = $presupuesto->total*0.50;
        $presupuesto->forma = $request->input('forma');
        $presupuesto->UT = $request->input('UT');
        $presupuesto->cuotas = $request->input('cuotas');
        $presupuesto->reserva = $request->input('reserva');
        
        $Clienteproyecto = DB::table('cliente_proyecto')->where('cedula_cliente','=',$cedula)->get();
        foreach ($Clienteproyecto as $value) {
            $Project = proyecto::where('id', $value->id_proyecto)->get();
        }
        
        $presupuesto->codigo = $this->generateCode($this->getProject($Clienteproyecto, $Project));
        $presupuesto->save();
        foreach ($Clienteproyecto as $Cproyecto) 
        {

            foreach($Project as $Proyecto)
            {

                if($Proyecto->id == $Cproyecto->id_proyecto)
                {
                    if($Proyecto->estado == "Planteado")
                    {
                    $ProyectoPresupuesto = new ProyectoPresupuesto;
                    
                    DB::table('proyecto')
                    ->where('id', $Cproyecto->id_proyecto)
                    ->update(['estado' => "En proceso de pago"]);
            
                    $ProyectoPresupuesto->id_proyecto = $Proyecto->id;
                    $ProyectoPresupuesto->id_presupuesto = $presupuesto->id;

                    $banco = $request->input('banco');
                    $ref = $request->input('ref');

                    $this->makePayment($cedula, $presupuesto->cuotas, $presupuesto->total, $presupuesto->forma, $Proyecto->producto, $presupuesto->reserva, $banco, $ref);

                    $ProyectoPresupuesto->save();
                    }
                }
            }

        }

        

        return redirect('pdf/'.$ProyectoPresupuesto->id_presupuesto);
    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function getProject($ClienteProyecto, $Project)
    {
        foreach ($ClienteProyecto as $Cproyecto)
        {

            foreach($Project as $Proyecto)
            {
                if($Proyecto->id == $Cproyecto->id_proyecto)
                {
                    if($Proyecto->estado == "Planteado")
                    {
                        
                        if($Proyecto->producto != "cocina")
                            return 'closet';
                        else
                           return 'cocina';
                    }
                        
                }       
                    
            }
                 
        }
            
    }

    public function generateCode($producto)
    {
        if($producto == 'closet')
            $proyectos = proyecto::where('producto','closet')->where('producto','vestier')->get();
        if($producto == 'cocina')
            $proyectos = proyecto::where('producto','cocina')->get();

        

        $presupuestos = array();
        $presupuestosyear = array();
        $thisYear = Carbon::now('y');
        $Q = 1;

        foreach ($proyectos as $key) {
            $presupuestos[$Q] = ProyectoPresupuesto::where('id_proyecto', $key->id)->get();
            if($presupuestos[$Q] != NULL)
                $Q++;
        }

        $date = new DateTime();

        foreach ($presupuestos as $pre) {
            foreach ($pre as $value) {
                $date = new DateTime($value->created_at);
                if($date->format('Y') == $thisYear)
                    $Q++;
            }
        }

        if($Q > 9)
            $zeros = "00";
        else
            $zeros = "000";

        if($producto == 'cocina')
        {
            $code = 'K-'.mb_strimwidth(Carbon::now('Y'), 2, 2).'/'.$zeros.$Q;
        }
        else
        {
            $code = 'C-'.mb_strimwidth(Carbon::now('Y'), 2, 2).'/'.$zeros.$Q;
        }

        return $code;

    }

    public function makePayment($cedula, $cuotas, $total, $forma, $producto, $reserva, $banco, $ref)
    {
        date_default_timezone_set('UTC');
        $Date = new DateTime();
       // $Date = $Date->format('d-m-y');

        $CarbonDate = Carbon::now();
     //   $CarbonDate = $CarbonDate->format('d-m-Y');
        $CarbonCuotas = array();
        for ($i=2; $i <=$cuotas ; $i++) { 
            
            $CarbonCuotas[$i] = Carbon::now()->AddDays(($i - 1) * 30);
    //        $CarbonCuotas[$i] = $CarbonCuotas[$i]->format('d-m-Y');

        }

        if($cuotas == 1)
        {
            $pago = new pagos;
            $clientePago = new clientePago;

            if($reserva > 0)
            {
                $concepto = 'reserva '.$producto;
                $restante = $this->abono($reserva, $concepto, $CarbonDate, $total, $forma, $cedula, $banco, $ref);

                $pago->abono = $restante;
                $pago->fecha = Carbon::now()->AddDays(10);
                $pago->concepto = 'Cuota número 1 de '.$producto;
                $pago->estado = 'pendiente';
                $pago->save();

                $clientePago->cedula_cliente = $cedula;
                $clientePago->id_pago = $pago->id;
                $clientePago->save();
            }
            else
            {
                $concepto = 'pago único de '.$producto;
                $this->abono($total, $concepto, $CarbonDate, $total, $forma, $cedula, $banco, $ref);
            }
        }
        if($cuotas > 1)
        {
            $arrayPagos = array();
            $clientePago = array();
            

            if($reserva > 0)
            {
                $concepto = 'reserva '.$producto;
                $inicial = $this->abono($reserva, $concepto, $CarbonDate, $total/$cuotas, $forma, $cedula, $banco, $ref);
                $estado_inicial = 'pendiente';
                $fecha_inicial = Carbon::now()->AddDays(10);
            
            }
            else
            {

                $inicial = $total/$cuotas;
                $estado_inicial = 'cancelado';
                $fecha_inicial = Carbon::now();
            }
                

            for($i = 1; $i<=$cuotas; $i++)
            {
                $arrayPagos[$i] = new pagos;
                $clientePago[$i] = new clientePago;


                if($i == 1)
                {

                    $arrayPagos[$i]->abono = $inicial;
                    $arrayPagos[$i]->forma = $forma;
                    $arrayPagos[$i]->fecha = $fecha_inicial;
                    $arrayPagos[$i]->fecha_abono = $Date;
                    $arrayPagos[$i]->estado = $estado_inicial;
                    $arrayPagos[$i]->concepto = 'Cuota número'.$i.' de '.$producto;
                  //  $arrayPagos[$i]->banco = $banco;
                  //  $arrayPagos[$i]->ref = $ref;
                    $arrayPagos[$i]->save();

                    $clientePago[$i]->id_pago = $arrayPagos[$i]->id;
                    $clientePago[$i]->cedula_cliente = $cedula;
                    $clientePago[$i]->save();
                }
                else
                {
                    $arrayPagos[$i]->abono = $total/$cuotas;
                    $days = ($i - 1) * 30;
                    $arrayPagos[$i]->fecha = Carbon::now()->AddDays(($i - 1) * 30);
                    //dd($CarbonCuotas[$i]);
                    $arrayPagos[$i]->concepto = 'Cuota número'.$i.' de '.$producto;
                    $arrayPagos[$i]->save();
                    $clientePago[$i]->id_pago = $arrayPagos[$i]->id;
                    $clientePago[$i]->cedula_cliente = $cedula;
                    $clientePago[$i]->save();

                }       
            }
        }

    }

    public function abono($monto, $concepto, $date, $total, $forma, $cedula, $banco, $ref)
    {
        $pago = new pagos;
        $clientePago = new clientePago;

        $pago->abono = $monto;
        $pago->concepto = $concepto;
        $pago->forma = $forma;
        $pago->fecha = $date;
        $pago->fecha_abono = $date;
        $pago->estado = 'cancelado';
        $pago->banco = $banco;
        $pago->ref = $ref;

        $pago->save();
        $clientePago->cedula_cliente = $cedula;
        $clientePago->id_pago = $pago->id;
        $clientePago->save();

        $restante = $total - $monto;
        return $restante;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $proyecto = ProyectoPresupuesto::where('id_proyecto', $id)->get();
        $proyectoData = Proyecto::where('id',$id)->get();
        foreach ($proyecto as $key) {
            $presupuesto = presupuesto::where('id', $key->id_presupuesto)->get();
        }
        $Cliente = clienteProyecto::where('id_proyecto', $id)->get();
        foreach ($Cliente as $key) {
            $ClienteData = cliente::where('cedula', $key->cedula_cliente)->get();
        }

        
        return view('presupuestoView', ['proyecto' => $proyectoData, 'presupuesto' => $presupuesto, 'cliente' => $ClienteData]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
