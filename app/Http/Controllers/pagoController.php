<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\pagos;
use App\clientePago;
use App\cliente;
use App\notification;
use App\pago_notification;
use DB;
use Carbon\Carbon;
use Datetime;

class pagoController extends Controller
{
    public function index()
    {
        //
    }

    public function update()
    {
        $cont = 0;
        $today = Carbon::now();
        $notification = pago_notification::all();
        $pagos = pagos::where('estado', 'pendiente')->get();
        foreach ($pagos as $key) {
            if($key->fecha < $today)
            {
                foreach ($notification as $value) {
                    if($value->id_pago == $key->id)
                    {
                        $cont++;
                    }
                }
                if($cont == 0)
                {
                    $notify = new notification;
                    $nombre = $this->getCliente($key->id);

                    $notify->descripcion = "Factura vencida del cliente ".$nombre.": ".$key->concepto;
                    $notify->save();

                    $saveNotify = new pago_notification;
                    $saveNotify->id_pago = $notify->id;
                    $saveNotify->id_pago = $key->id;
                    $saveNotify->save();

                    $pagosVencidos = pagos::find($key->id);
                    $pagosVencidos->estado = "vencido";
                    $pagosVencidos->save();
                }
            }
            $cont = 0;
        }
    }


    public function Store(Request $request, $id)
    {


        $total = pagos::where('id',$id)->get();

        foreach ($total as $value) {
            $deuda = $value->abono;
            $fecha = $value->fecha;
        }

        $cliente = clientePago::where('id_pago',$id)->get();
        foreach ($cliente as $ci) 
        {
            $cedula = $ci->cedula_cliente;
        }

        if($deuda == (float)$request->input("cantidad"))
        {  
            DB::table('pagos')
                    ->where('id', '=', $id)
                    ->update(['estado' => "cancelado",
                        'fecha_abono' => $request->input("fecha"),
                        'concepto' => $request->input("concepto"),
                        'forma' => $request->input("metodo"),
                        'banco' => $request->input("banco"),
                        'ref' => $request->input('ref')
                        ]);
                $recibo = $id;
        }
        else
        {
            $pago = new pagos;
            $pago->fecha = $fecha;
            $pago->estado = 'cancelado';
            $pago->fecha_abono = Carbon::now();
            $pago->abono = $request->input("cantidad");
            $pago->forma = $request->input("metodo");
            $pago->concepto = $request->input("concepto");
            $pago->banco = $request->input("banco");
            $pago->ref = $request->input("ref");
            $pago->save();

            $ClientePago = new clientePago;
            $ClientePago->cedula_cliente = $cedula;
            $ClientePago->id_pago = $pago->id;
            $ClientePago->save();

            $saldo = $deuda - $pago->abono;

            DB::table('pagos')
                    ->where('id', '=', $id)
                    ->update(['abono' => $saldo
                        ]);

            $recibo = $pago->id;
        } 

        $this->notify($cedula);

        $pagosCancelados = pagos::where('estado', 'cancelado')->take(6)->orderBy('fecha_abono','desc')->get();
        $pagosPendientes = pagos::where('estado', 'pendiente')->orderBy('fecha','asc')->get();
        $cPago = clientePago::all();
        $cliente = cliente::all();

        if(isset($_POST['si']))
            return redirect('recibos/'.$recibo);
        else
            return view('pagoIndex', ['pagosC' => $pagosCancelados, 'pagosP' => $pagosPendientes, 'cPago' => $cPago, 'cliente' => $cliente]);
    }

    public function show()
    {
        $this->update();
        $pagosCancelados = pagos::where('estado', 'cancelado')->take(6)->orderBy('fecha_abono','desc')->get();
        $pagosPendientes = pagos::where('estado', 'pendiente')->orderBy('fecha','asc')->get();
        $cPago = clientePago::all();
        $cliente = cliente::all();

        return view("pagoIndex", ['pagosC' => $pagosCancelados, 'pagosP' => $pagosPendientes, 'cPago' => $cPago, 'cliente' => $cliente]);
    }

    public function pay($id)
    {
        $this->update();
        $pagos = pagos::where("id", $id)->get();
        return view("PagoEdit", ['pagos' => $pagos]);
    }

    public function viewAll()
    {
        $this->update();
        $pagos = pagos::where('estado', 'cancelado')->orderBy('fecha', 'desc')->get();
        $cPago = clientePago::all();
        $cliente = cliente::all();

        return view("viewPagos", ['pagos' => $pagos, 'cPago' => $cPago, 'cliente' => $cliente]);
    }

    public function filterByDate(Request $request)
    {
        $m = $request->input('month');
        $date = new DateTime();
        $date->setDate(2016, $m, 3);
        $pagos = pagos::whereMonth('fecha', '=', $date->format('m'))->orderBy('fecha', 'desc')->get();
        $cPago = clientePago::all();
        $cliente = cliente::all();

        return view("viewPagos", ['pagos' => $pagos, 'cPago' => $cPago, 'cliente' => $cliente]);
    }

    public function filterByClient(Request $request)
    {
        $c = $request->input('cliente');
        $cPago = clientePago::where('cedula_cliente', $c)->get();
        
        if($cPago == NULL)
        {
            $cedulas= array();
            $cliente = cliente::where('nombre','like',$c)->get();
            foreach ($cliente as $key) {
                array_push($cedulas, $key->cedula);
            }
            $cPago = clientePago::whereIn('cedula_cliente', $cedulas)->get();

        }
        $p = pagos::all();
        $id_pago = array();
        $cont = 0;



        foreach ($cPago as $key) {
            foreach ($p as $value) {
                if($key->id_pago == $value->id)
                {
                    $id_pago[$cont] = $value->id;
                    $cont++; 
                }
            }
            
        }

        $pagos = pagos::whereIn('id', $id_pago)->where('estado','cancelado')->get();
        
        $cliente = cliente::all();

        return view("viewPagos", ['pagos' => $pagos, 'cPago' => $cPago, 'cliente' => $cliente]);
    }

    public function getCliente($id)
    {
        $clientePago = clientePago::where('id_pago', $id)->get();
        $client = cliente::all();
        foreach ($clientePago as $cPago) {
            
            foreach ($client as $ident) {
                if($ident->cedula == $cPago->cedula_cliente)
                    $nombre = $ident->nombre;
                       
            }
            
        }
        return $nombre;
    }


    public function notify($cliente)
    {
        $datos = DB::table('cliente')->where('cedula',$cliente)->get();
        foreach ($datos as $key) {
            $nombre = $key->nombre;
        }

        $notification = new notification;
        $notification->descripcion = 'Nuevo pago registrado del cliente '.$nombre;
        $notification->save();
    }
}
