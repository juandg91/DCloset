<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;

use App\Http\Requests;
use App\Http\Requests\PostsRequest;
use App\Http\Controllers\Controller;
use App\citaMedida;
use App\ClienteCita;
use App\cliente;
use App\citaPresupuesto;
use App\clienteCitaPresupuesto;
use App\notification;

class citasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Return view('Citas/citaMain');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createCitaMedida()
    {
        return view('cita');
    }

    public function createCitaMedidaC($cedula)
    {
        return view('cita', ['cedula' => $cedula]);
    }


    public function createCitaPresupuesto($cedula)
    {
        return view('Citas/citaPresupuesto', ['cedula' => $cedula]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeCitaMedida(PostsRequest $request)
    {
        $citaMedida = new citaMedida;
        $citaMedida->fecha = $request->input('fecha');
        $citaMedida->hora = $request->input('hora');
        $citaMedida->obs = $request->input('obs');

        $citaMedida->save();

        $ClienteCita = new ClienteCita;
        $ClienteCita->cedula_cliente = $request->input('cedula');
        $ClienteCita->id_cita = $citaMedida->id;

        $ClienteCita->save();

        return redirect('/citasShow');
    }

    public function storeCitaPresupuesto(PostsRequest $request)
    {
        $citaPresupuesto = new citaPresupuesto;
        $citaPresupuesto->fecha = $request->input('fecha');
        $citaPresupuesto->hora = $request->input('hora');
        $citaPresupuesto->obs = $request->input('obs');

        $citaPresupuesto->save();

        $clienteCitaPresupuesto = new clienteCitaPresupuesto;
        $clienteCitaPresupuesto->cedula_cliente = $request->input('cedula');
        $clienteCitaPresupuesto->id_cita = $citaPresupuesto->id;

        $clienteCitaPresupuesto->save();

        return view('Citas/citaPresupuesto');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $ClienteCita = ClienteCita::all();
        $citaMedida = citaMedida::all();
        $cliente = cliente::all();
        return view('Citas/citaView', ['cita' => $citaMedida, 'cliente_cita' => $ClienteCita,'cliente' => $cliente, 'tipo' => "medidas"]);
    }

    public function showPending()
    {
        $ClienteCita = ClienteCita::all();
        $citaMedida = citaMedida::where('estado', 'en espera')->get();
        $cliente = cliente::all();
        return view('Citas/citaView', ['cita' => $citaMedida, 'cliente_cita' => $ClienteCita,'cliente' => $cliente, 'tipo' => "medidas"]);
    }

    public function showExecuted()
    {
        $ClienteCita = ClienteCita::all();
        $citaMedida = citaMedida::where('estado', 'Realizada')->get();
        $cliente = cliente::all();
        return view('Citas/citaView', ['cita' => $citaMedida, 'cliente_cita' => $ClienteCita,'cliente' => $cliente, 'tipo' => "medidas"]);
    }

    public function showDateOrder()
    {
        $ClienteCita = ClienteCita::all();
        $citaMedida = DB::table('cita')->orderBy('created_at','desc')
        ->get();
        $cliente = cliente::all();
        return view('Citas/citaView', ['cita' => $citaMedida, 'cliente_cita' => $ClienteCita,'cliente' => $cliente, 'tipo' => "medidas"]);
    }

    public function showPresupuestos()
    {
        $clienteCitaPresupuesto = clienteCitaPresupuesto::all();
        $citaPresupuesto = citaPresupuesto::all();
        $cliente = cliente::all();
        return view('Citas/citaView', ['cita' => $citaPresupuesto, 'cliente_cita' => $clienteCitaPresupuesto,'cliente' => $cliente, 'tipo' => "presupuesto"]);
    }

    public function showPresupuestoPending()
    {
        $clienteCitaPresupuesto = clienteCitaPresupuesto::all();
        $citaPresupuesto = citaPresupuesto::where('estado', 'en espera')->get();
        $cliente = cliente::all();
        return view('Citas/citaView', ['cita' => $citaPresupuesto, 'cliente_cita' => $clienteCitaPresupuesto,'cliente' => $cliente, 'tipo' => "presupuesto"]);
    }

    public function showPresupuestoExecuted()
    {
        $clienteCitaPresupuesto = clienteCitaPresupuesto::all();
        $citaPresupuesto = citaPresupuesto::where('estado', 'Realizada')->get();
        $cliente = cliente::all();
        return view('Citas/citaView', ['cita' => $citaPresupuesto, 'cliente_cita' => $clienteCitaPresupuesto,'cliente' => $cliente, 'tipo' => "presupuesto"]);
    }

    public function showPresupuestoOrdered()
    {
        $clienteCitaPresupuesto = clienteCitaPresupuesto::all();
        $citaPresupuesto = DB::table('citapresupuesto')->orderBy('created_at','desc')->get(); 
        $cliente = cliente::all();
        return view('Citas/citaView', ['cita' => $citaPresupuesto, 'cliente_cita' => $clienteCitaPresupuesto,'cliente' => $cliente, 'tipo' => "presupuesto"]);
    }

    public function searchPresupuesto(PostsRequest $request)
    {
        $citaPresupuesto = citaPresupuesto::all();
        $cliente = cliente::all();
        $data = $request->input('search');
        $user_find = DB::table('cliente_citapresupuesto')->where('cedula_cliente', '=', $data)->get();
        
        return view('Citas/citaView', ['cita' => $citaPresupuesto, 'cliente_cita' => $user_find,'cliente' => $cliente, 'tipo' => "presupuesto"]);
    }

    public function searchCita(PostsRequest $request)
    {
        $cita = citaMedida::all();
        $cliente = cliente::all();
        $data = $request->input('search');
        $user_find = DB::table('cliente_cita')->where('cedula_cliente', '=', $data)->get();
        
        return view('Citas/citaView', ['cita' => $cita, 'cliente_cita' => $user_find,'cliente' => $cliente, 'tipo' => "medidas"]);
    }

    public function edit($cedula, $id)
    {
        $cliente = cliente::all();
        $user_find = DB::table('cliente')->where('cedula', '=', $cedula)->get();

        return view('Citas/editarCita', ['cedula' => $cedula, 'cliente' => $user_find, 'id' => $id, 'type' => "medidas"]);
    }

    public function editConfirm($cedula, $id, PostsRequest $request)
    {
/*        $ClienteCita = DB::table('cliente_cita')->where('id_cita', '=', $id)->get();
        foreach ($ClienteCita as $key => $value) {
            
            $id = $value->id_cita; 
        }*/

        if($request->input('status') != "pospuesta")
        {
            DB::table('cita')
            ->where('id', $id)
            ->update(['estado' => $request->input('status'), 'obs' => $request->input('obs')]);
        }

        if($request->input('status') == "pospuesta")
        {
            DB::table('cita')
            ->where('id', $id)
            ->update(['estado' => $request->input('status'), 'obs' => $request->input('obs'), 'fecha' => $request->input('fecha')]);
        }

        return view('Citas/citaMain');
        
    }

    public function editP($cedula, $id)
    {
        $cliente = cliente::all();
        $user_find = DB::table('cliente')->where('cedula', '=', $cedula)->get();

        return view('Citas/editarCita', ['cedula' => $cedula, 'cliente' => $user_find, 'id' => $id, 'type' => "presupuesto"]);
    }

    public function editpConfirm($cedula, $id, PostsRequest $request)
    {
/*        $ClienteCita = DB::table('cliente_cita')->where('id_cita', '=', $id)->get();
        foreach ($ClienteCita as $key => $value) {
            
            $id = $value->id_cita; 
        }*/

        if($request->input('status') != "pospuesta")
        {
            DB::table('citapresupuesto')
            ->where('id', $id)
            ->update(['estado' => $request->input('status'), 'obs' => $request->input('obs')]);
        }

        if($request->input('status') == "pospuesta")
        {
            DB::table('citapresupuesto')
            ->where('id', $id)
            ->update(['estado' => $request->input('status'), 'obs' => $request->input('obs'), 'fecha' => $request->input('fecha')]);
        }

        return view('Citas/citaMain');
        
    }

    Public function nuevaCitaPresupuesto()
    {
        return view('NuevaCitaPresupuesto');
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
