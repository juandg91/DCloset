<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use Carbon\Carbon;
use dateTime;

use App\Http\Requests;
use App\Http\Requests\PostsRequest;
use App\Http\Controllers\Controller;
use App\cliente;
use App\proyecto;
use App\proyecto_ejecucion;
use App\clienteProyecto;
use App\linea;
use App\producto;
use App\color;
use App\ProyectoPresupuesto;
use App\garantia;
use App\proyecto_cliente_garantia;

class proyectoController extends Controller
{
    public function index()
    {
        $clientes = cliente::all();
        $inter = clienteProyecto::all();
        $proyecto = proyecto::where('estado', '!=', 'Anulado')
               ->get();
        $contratos = ProyectoPresupuesto::all();

        Return view('ProyectosIndex', ['proyectos' => $proyecto, 'clientes' => $clientes, 'inter' => $inter, 'budget' => $contratos]);
    }
  
    public function create()
    {
        Return view('NuevoProyecto');

    }

    public function crear($cedula)
    {
    
        $data = linea::all();
        #$data = DB::table('linea')->get();
        $product = producto::all();
        
        Return view('NuevoProyecto',['cedula' => $cedula, 'linea' => $data, 'producto' => $product]);

    }
    
    public function getColors(Request $request, $linea, $product)
    {
        if($request->ajax())
        {
            $colors = color::getColors($linea, $product);
            return response()->json($colors);
        }
    }

    public function store(Request $request, $cedula)
    {
        $Proyecto = new proyecto;
        $Proyecto->nombre = $request->input('nombre');
        $Proyecto->subtotal = $request->input('subtotal');
        $Proyecto->impuesto = $request->input('impuesto');
        $Proyecto->producto = $request->input('product');
        $Proyecto->color = $request->input('color');
        $Proyecto->linea = $request->input('linea');
        $Proyecto->modelo = $request->input('modelo');

        if($Proyecto->producto == "cocina")
        {
            $Proyecto->estructura = $request->input('struct');
            $Proyecto->tope = $request->input('tope');

        }

        if($Proyecto->producto != "vestier")
            $Proyecto->puerta = $request->input('puertas');

        $Proyecto->manillas = $request->input('manillas');
        


        $Proyecto->obs = $request->input('obs');
        
        $Proyecto->total = $request->input('total');

        $Proyecto->save();

        $Cliente = new clienteProyecto;
        $Cliente->cedula_cliente = $cedula;
        $Cliente->id_proyecto = $Proyecto->id;

        $Cliente->save();

        $clientes = cliente::all();
        $inter = clienteProyecto::all();
        $proyecto = proyecto::where('estado', '!=', 'Anulado')
               ->get();
        $contratos = ProyectoPresupuesto::all();

        Return view('ProyectosIndex', ['proyectos' => $proyecto, 'clientes' => $clientes, 'inter' => $inter, 'budget' => $contratos]);

        
    }
    
    public function edit($id)
    {
        $proyecto = DB::table('proyecto')->where('id', '=', $id)->get();
        $inter = DB::table('cliente_proyecto')->where('id_proyecto', '=', $id)->get();

        foreach ($inter as $client) {
            
            if($client->id_proyecto == $id)
            {
                $ci = $client->cedula_cliente;
            }
        }

        $cliente = DB::table('cliente')->where('cedula','=',$ci)->get();

        $CP = DB::table('proyecto_cp')->where('id_proyecto','=',$id)->get();

        Return view('ProyectoEdit', ['proyecto' => $proyecto, 'clienteData' => $cliente, 'CPdata' =>$CP]);
    }

    public function update($id)
    {
        $proyecto = proyecto::where('id', '=', $id)
               ->get();
        $data = linea::all();
        Return view('ProyectoUpdate', ['project' => $proyecto, 'linea' => $data]);
    }

    public function saveUpdate(Request $request, $id)
    {
        $Proyecto = new proyecto;
        $producto = $request->input('product');
        $Proyecto->nombre = $request->input('nombre');
        $Proyecto->subtotal = $request->input('subtotal');
        $Proyecto->impuesto = $request->input('impuesto');
        $Proyecto->color = $request->input('color');
        $Proyecto->linea = $request->input('linea');
        $Proyecto->modelo = $request->input('modelo');

        if($producto == "cocina")
        {
            $Proyecto->estructura = $request->input('struct');
            $Proyecto->tope = $request->input('tope');

        }

        
        $Proyecto->puerta = $request->input('puertas');
        $Proyecto->manillas = $request->input('manillas');
        


        $Proyecto->obs = $request->input('obs');
        
        $Proyecto->total = $request->input('total');

        if($producto == "cocina")
        {
            DB::table('proyecto')
                    ->where('id', '=', $id)
                    ->update(['nombre' => $Proyecto->nombre,
                        'subtotal' => $Proyecto->subtotal,
                        'impuesto' => $Proyecto->impuesto,
                        'total' => $Proyecto->total,
                        'color' => $Proyecto->color,
                        'linea' => $Proyecto->linea,
                        'modelo' => $Proyecto->modelo,
                        'estructura' => $Proyecto->estructura,
                        'tope' => $Proyecto->tope,
                        'puerta' => $Proyecto->puerta,
                        'manillas' => $Proyecto->manillas,
                        'obs' => $Proyecto->obs]);
        }
        if($producto == "closet")
        {
            DB::table('proyecto')
                    ->where('id', '=', $id)
                    ->update(['nombre' => $Proyecto->nombre,
                        'subtotal' => $Proyecto->subtotal,
                        'impuesto' => $Proyecto->impuesto,
                        'total' => $Proyecto->total,
                        'color' => $Proyecto->color,
                        'linea' => $Proyecto->linea,
                        'modelo' => $Proyecto->modelo,
                        'puerta' => $Proyecto->puerta,
                        'manillas' => $Proyecto->manillas,
                        'obs' => $Proyecto->obs]);
        }
        if($producto == "vestier")
        {
            DB::table('proyecto')
                    ->where('id', '=', $id)
                    ->update(['nombre' => $Proyecto->nombre,
                        'subtotal' => $Proyecto->subtotal,
                        'impuesto' => $Proyecto->impuesto,
                        'total' => $Proyecto->total,
                        'color' => $Proyecto->color,
                        'linea' => $Proyecto->linea,
                        'modelo' => $Proyecto->modelo,
                        'manillas' => $Proyecto->manillas,
                        'obs' => $Proyecto->obs]);
        }
        $stringRoute = "/proyecto/edit/".$id;
        
        return redirect($stringRoute);
    }

    public function delete($id)
    {
        DB::table('proyecto')
                    ->where('id', '=', $id)
                    ->update(['estado' => "Anulado"]);

         return redirect('/proyecto');
    }

    public function execute($id)
    {
        $proyecto = proyecto::where('id', '=', $id)
               ->get();
        $inter = DB::table('cliente_proyecto')->where('id_proyecto', '=', $id)->get();

        foreach ($inter as $client) {
            
            if($client->id_proyecto == $id)
            {
                $ci = $client->cedula_cliente;
            }
        }

        $cliente = DB::table('cliente')->where('cedula','=',$ci)->get();

        $CP = DB::table('proyecto_cp')->where('id_proyecto','=',$id)->get();

         return view('executeScreen', ['proyecto' => $proyecto, 'clienteData' => $cliente, 'CPdata' =>$CP]);
    }

    public function WarrantyDate($date, $time)
    {
        $date = new dateTime($date);
        if($time = "1 año")
        {
              return  Carbon::createFromDate(date_format($date, 'Y'), date_format($date, 'm'), date_format($date, 'd'))->addYears(1);
        }
        if($time = "6 meses")
        {
            
            return  Carbon::createFromDate(date_format($date, 'Y'), date_format($date, 'm'), date_format($date, 'd'))->addDays(180);
        }
    }

    public function executeSave(Request $request, $id)
    {
        DB::table('proyecto')
                    ->where('id', '=', $id)
                    ->update(['estado' => "Instalado", 'obs' => $request->input('obs')]);

        $ejecucion = new proyecto_ejecucion;
        $ejecucion->id_proyecto = $id;
        $ejecucion->fecha = $request->input('date');
        $ejecucion->save();

        $NuevaGarantia = new garantia;
        $NuevaGarantia->fecha = $this->WarrantyDate($request->input('date'), $request->input('time'));
        $NuevaGarantia->estado = "Vigente";
        $NuevaGarantia->save();


        $proyecto = DB::table('proyecto')->where('id', '=', $id)->get();
        $inter = DB::table('cliente_proyecto')->where('id_proyecto', '=', $id)->get();

        foreach ($inter as $client) {
            
            if($client->id_proyecto == $id)
            {
                $ci = $client->cedula_cliente;
            }
        }

        $cliente = DB::table('cliente')->where('cedula','=',$ci)->get();

        $CP = DB::table('proyecto_cp')->where('id_proyecto','=',$id)->get();



        $aux = new proyecto_cliente_garantia;
        $aux->id_proyecto = $id;
/*zona critica*/        $aux->cedula_cliente = $ci;
        $aux->id = $NuevaGarantia->id;
        $aux->save();

        

        return redirect('/proyecto');
    }

}
