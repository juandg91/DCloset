<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class pdfController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }
    public function getProjects($id)
    {
        $cont = 0;
        $ProjectArray = array();
        $projects = DB::table("proyecto_presupuesto")->where("id_presupuesto", "=", $id)->get();
        foreach ($projects as $current) 
        {

            
                $token = DB::table("proyecto")->where("id", "=", $current->id_proyecto)->get();
                foreach ($token as $key) 
                {
                    # code...
                    if($key->estado != "Planteado" && $key->estado != "Anulado")
                    {
                        $ProjectArray[$cont] = DB::table("proyecto")->where("id", "=", $current->id_proyecto)->get();
                        $cont++;
                    }
                }   
            
        }
        
        return $ProjectArray;
    }

    public function getPrice($id)
    {
        $price = DB::table("presupuesto")->where("id","=",$id)->get();

        return $price;
    }
    public function findCliente($id)
    {
        $projects = DB::table("proyecto_presupuesto")->where("id_presupuesto", "=", $id)->get();
        foreach ($projects as $current) {
            
            $cProject = DB::table("cliente_proyecto")->where("id_proyecto", "=", $current->id_proyecto)->get();
            
        }

        foreach ($cProject as $current) {
            $cliente = DB::table("cliente")->where("cedula","=",$current->cedula_cliente)->get();
        }
        return $cliente;
    }

    public function invoice($id)
    {

        $projects = $this->getProjects($id);
        $price = $this->getPrice($id);
        $cliente = $this->findCliente($id);
     
      $pdf = \App::make('dompdf.wrapper');
//      $pdf->set_base_path('/public/css/contratoStyle.css');
//      $pdf->set_base_path("/public/assets/css/contratoStyle.css");
      $view = \View::make('contract', compact('projects','price','cliente'));
     
        $pdf->loadHTML($view);
        
        return $pdf->stream();
     
    }

    public function getPago($id)
    {
        $pagos = DB::table('pagos')->where('id', $id)->get();
        return $pagos;
    }

    public function findPagoCliente($id)
    {
        $clientePago = DB::table('cliente_pago')->where('id_pago',$id)->get();
        foreach ($clientePago as $key) {
            $cliente = DB::table('cliente')->where('cedula',$key->cedula_cliente)->get();
            # code...
        }
        return $cliente;
    }

    public function deuda($cliente)
    {
        $suma = 0;

        foreach ($cliente as $key) {
          $cedula = $key->cedula;
        }

        $clientePago = DB::table('cliente_pago')->where('cedula_cliente',$cedula)->get();

         foreach ($clientePago as $key) {
            $id = DB::table('pagos')->where('id',$key->id_pago)->where('estado','pendiente')->get();
            # code...
            foreach ($id as $value) {
              $suma = $suma + $value->abono;
            }
        }
        
        return $suma;
    }

    public function reciboPago($id)
    {
        $pago = $this->getPago($id);
        $cliente = $this->findPagoCliente($id);
        $deuda = $this->deuda($cliente);
        
      $pdf = \App::make('dompdf.wrapper');

      $view = \View::make('recibo', compact('pago','cliente', 'deuda'));
     
        $pdf->loadHTML($view);
        
        return $pdf->stream();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
