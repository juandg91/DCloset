<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use App\project_content;
use App\proyecto_cp;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class CPController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function nuevo()
    {
        return view('CP');
    }

    public function getParts()
    {
        $parts = array();

        $parts = array(

            'CTFMBANG' => 0,
            'PSWEN914' => 0,
            'ENTDS30' => 0,
            'ENTDS45' => 0,
            'ENTMB45' => 0,
            'ENTMB60' => 0,
            'ENTMB90' => 0,
            'ENTMBANG' => 0,
            'ENTTH60' => 0,
            'ENTTH90' => 0,
            'ENTW45' => 0,
            'FDMB45' => 0,
            'FDMB30' => 0,
            'FDMB60' => 0,
            'FDMB90' => 0,
            'FDDS30' => 0,
            'FDDS45' => 0,
            'FDMBANG' => 0,
            'FDMBF60' => 0,
            'FDMBF90' => 0,
            'FDTH60' => 0,
            'FDTH90' => 0,
            'FDW45' => 0,
            'FDW60' => 0,
            'FDW90' => 0,
            'FDWEN80' => 0,
            'GV60' => 0,
            'GV90' => 0,
            'GVT60' => 0,
            'GVT90' => 0,
            'LTEN' => 0,
            'LTMB' => 0,
            'LTMBANG' => 0,
            'LTT' => 0,
            'LTW' => 0,
            'LTW72' => 0,
            'PDS30' => 0,
            'PDS45' => 0,
            'PMB30' => 0,
            'PMB45' => 0,
            'PMB60' => 0,
            'PSMB30' => 0,
            'PSMB45' => 0,
            'PSMB60' => 0,
            'PSMB90' => 0,
            'PSMBANG' => 0,
            'PSTH60' => 0,
            'PSTH90' => 0,
            'PSW45' => 0,
            'PSW60' => 0,
            'PSW90' => 0,
            
            'PSWEN80' => 0,
            'PW60' => 0,
            'FDWEN914' => 0,
            'TCWEN914' => 0,
            'TCEN914' => 0,
            'PWEN914' => 0,
            'PWEN80' => 0,
            'PWTH90' => 0,
            'TCMB45' => 0,
            'TCMB30' => 0,
            'TCMB60' => 0,
            'TCMB90' => 0,
            'TCMBANG' => 0,
            'TCMBF90' => 0,
            'TCMBG60' => 0,
            'TCMBF60' => 0,
            'TCTH60' => 0,
            'TCTH90' => 0,            
            'TCW45' => 0,
            'TCW60' => 0,
            'TCW90' => 0,
            'TCWEN80' => 0

            );
      #  dd($parts['PSWEN914']);
        return $parts;
    }

    public function getComps()
    {
         $comps = array(
            'BISAGRAS' => 0,
            'CANTO BLANCO' => 0,
            'GATOS HIDRAULICOS' => 0,
            'PATAS' => 0,
            'SISTEMAS HF' => 0,
            'TORNILLOS FABRICAR' => 0,
            'TORNILLOS PARA FIJAR' => 0,
            'TORNILLOS PATAS' => 0,
            'METROS DE TOPE' => 0
            );

         return $comps;
    }

    public function getData(Request $request, $id)
    {
        if($id == NULL)
        {
        $pieces = array(
        'mueble base 30' => $request->input('mb30'), 
        'mueble base 45' => $request->input('mb45'),
        'mueble base 60' => $request->input('mb60'),
        'mueble base 90' => $request->input('mb90'),
        'despensa 30' => $request->input('d30'),
        'despensa 45' => $request->input('d45'),
        'despensa 60' => $request->input('d60'),
        'mueble base fregaplato 60' => $request->input('mbf60'),
        'mueble base fregaplato 90' => $request->input('mbf90'),
        'mueble base gavetero 60' => $request->input('mbg60'),
        'mueble base angulo' => $request->input('mba'),
        'torre horno 60' => $request->input('th60'),
        'torre horno 90' => $request->input('th90'),
        'empotramiento de nevera 80' => $request->input('en80'),
        'empotramiento de nevera 914' => $request->input('en914'),
        'wall 45' => $request->input('w45'),
        'wall 60' => $request->input('w60'),
        'wall 90' => $request->input('w90'),
        'wall platera 60' => $request->input('wp60'),
        'wall platera 90' => $request->input('wp90'),
        'separadores' => $request->input('sp'),
        'retrobases' => $request->input('rb'),
        'desayunador' => $request->input('tpds'),
        'pantalla' => $request->input('tpp'),
        'adicional' => $request->input('tpa'),
        'superficie' => $request->input('tps') 

            ); 
        }
        
        else
        {
            $pieces = array();
            $pieces = $this->getParts();

            $fetching = DB::table('project_content')->where('id','=',$id)->get();
            
            foreach ($fetching as $key => $value) {
 #           dd($fetching);
            foreach ($value as $chain => $token) {
                    
                  
                if($chain != "id" && $token > 0)
                {



                    $mats = DB::table('bases_code')->where('nombre_base', '=', $chain)->get();
                    
                    foreach ($mats as $material) 
                   {    
                            
                            $pieces[$material->codigo] = $pieces[$material->codigo] + $material->cantidad;
                            
                   } 
                    
                }
               
            }
            
            }
        }
        
        
        return $pieces;   
    }

    public function getComponents($id)
    {
        $comps = $this->getComps();
        
        $fetching = DB::table('project_content')->where('id','=',$id)->get();

         foreach ($fetching as $key => $value) {

            foreach ($value as $chain => $token) {
                if($chain != "id" && $token > 0)
                {

                    $find_comp = DB::table('bases_components')->where('base', '=', $chain)->get();
                    
                  foreach ($find_comp as $get_comp) 
                   {
                        
                        $comps[$get_comp->component] = $comps[$get_comp->component] + $get_comp->cantidad;
                   }

                }

            }
         }

         return $comps;
    }

    public function generator(Request $request, $id)
    {
        #cantidades

        $parts = array();
        $comps = array();
        $parts = $this->getParts();
        $comps = $this->getComps();
        
        
        $pieces = $this->getData($request, NULL);

        foreach ($pieces as $current => $value) 
        {
            if($value > 0)
            {
                for($i=0; $i<$value; $i++)
                {
                   $find = DB::table('bases_code')->where('nombre_base', '=', $current)->get();
                   $find_comp = DB::table('bases_components')->where('base', '=', $current)->get(); 

                   foreach ($find as $key) 
                   {
                       # code foreach
                        #if($ItemTaken == 'mueble base 30')
                        #{                        
                            #code if

                            
                                $cant = $parts[$key->codigo];
                                $parts[$key->codigo] = $parts[$key->codigo] + $key->cantidad;

                            

                        #}

                         
                   }


                

                   foreach ($find_comp as $key) 
                   {
                       # code...
                        $comps[$key->component] = $comps[$key->component] + $key->cantidad;
                   }
                }
                
            }
        }
        $comps['METROS DE TOPE'] = $pieces['desayunador'] + $pieces['pantalla'] + $pieces['superficie'] + $pieces['adicional'];
        $project_content = new project_content;
        $cp = new proyecto_cp;

    #    $this->store($request, $pieces);

        foreach ($pieces as $key => $value) {
            
            $project_content->$key = $value;
        }

        $project_content->save();
        $cp->id_proyecto = $id;
        $cp->id_cp = $project_content->id;
        $cp->save();

        $proyecto = DB::table('proyecto')->where('id','=',$id)->get();

        return view('CPview', ['data' => $parts, 'comps' => $comps, 'proyecto' => $proyecto]);
    }

    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $array)
    {
        $cp = cp::all();

        foreach ($array as $key => $value) {
            $cp->$key = $value;
        }

        $cp->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $rq, $id)
    {
        $cp = DB::table('proyecto_cp')->where('id_proyecto','=',$id)->get();
        $proyecto = DB::table('proyecto')->where('id','=',$id)->get();

        if($cp != NULL)
        {
            foreach ($cp as $value) {
                $key = $value->id_cp;
            }

            $content1 = $this->getData($rq, $key);
            $content2 = $this->getComponents($key);
            
            

            return view('CPview', ['data' => $content1, 'comps' => $content2, 'proyecto' => $proyecto]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
