<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\cliente;
use App\citaMedida;
use App\citaPresupuesto;
use Carbon\Carbon;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\notification;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function notifications()
    {

        $notifications = DB::table('notification')->orderBy('created_at','desc')
        ->take(8)
        ->get();

        return $notifications;
    }

    public function cantidadClientes($today)
    {
        $cantClientes = cliente::all();
        $cant = 0;
        foreach($cantClientes as $cantidad)
        {
            $date =  $cantidad->created_at;
            $date = $date->format('d-m-Y');
            
            if($date == $today)
            {
                $cant++;
            }
        }
        return $cant;
    }

    public function cantidadCitasPresupuesto($today)
    {
        $citas = citaPresupuesto::where('estado', 'en espera')->get();
        $cant = 0;
        foreach($citas as $cantidad)
        {
            $date =  $cantidad->created_at;
            $date = $date->format('d-m-Y');
            
            if($date == $today)
            {
                $cant++;
            }
        }
        
        return $cant;
    }

    public function cantidadCitasMedidas($today)
    {
        $citas = citaMedida::where('estado', 'en espera')->get();
        $cant = 0;
        foreach($citas as $cantidad)
        {
            $date =  $cantidad->created_at;
            $date = $date->format('d-m-Y');
            
            if($date == $today)
            {
                $cant++;
            }
        }
        return $cant;
    }

    public function index()
    {
        
        $notify = $this->notifications();
        $today = carbon::now();
        $today = $today->format('d-m-Y');

        $cantidadClientes = $this->cantidadClientes($today);
        $cantidadCitas = $this->cantidadCitasMedidas($today);
        $cantidadCitasP = $this->cantidadCitasMedidas($today);

        return view('welcome',['notify' => $notify]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function cita_medidas()
    {
        return view('cita');
    }

    public function cita_presupuesto()
    {
        return view('citaPresupuesto');
    }

    public function assistant()
    {
        return view('posts.create');
    }

    public function pruebalogin()
    {
        return view('login');
    }

    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
